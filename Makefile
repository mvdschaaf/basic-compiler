objects = compiler.o parser.o init.o lexer.o error.o emitter.o symbol.o builtin/inputLexer.o builtin/exception.o
libobjects = builtin/print.o builtin/exception.o builtin/strings.o builtin/math.o builtin/arrays.o builtin/read.o builtin/input.o builtin/inputLexer.o
CC = gcc
CFLAGS = -Wall -pedantic -Werror

all : basicc $(libobjects)
.PHONY : all clean

basicc : $(objects)
	$(CC) -o basicc $(objects)

compiler.o : global.h init.h parser.h
parser.o : global.h lexer.h error.h emitter.h
init.o : global.h symbol.h
lexer.o : global.h conio.h error.h symbol.h
error.o : global.h
emitter.o : global.h error.h
symbol.o : global.h error.h

builtin/print.o : builtin/print.h builtin/exception.h
builtin/exception.o : builtin/exception.h
builtin/strings.o : builtin/strings.h
builtin/math.o : builtin/math.h
builtin/arrays.o : builtin/arrays.h
builtin/read.o : builtin/read.h
builtin/inputLexer.o : builtin/inputLexer.h builtin/exception.h
builtin/input.o : builtin/inputLexer.h builtin/exception.h

clean:
	rm basicc $(objects) $(libobjects)
