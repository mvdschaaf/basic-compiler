/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __ERROR__H_
#define __ERROR__H_

void error(char*);

#endif
