/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___EXCEPTION_H___
#define ___EXCEPTION_H___

void nonFatalError(char *,int);
void exception(char*,int);

#endif
