/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include <stdio.h>
#include <stdlib.h>
#include "inputLexer.h"
#include "exception.h"

#define MAX_DATA 32 

datum lexedData[MAX_DATA];
int dataLoc;

int isQuotedStringCharacter(char);
int isUnquotedStringCharacter(char);
int isPlainStringCharacter(char);
int isLetter(char);
int isDigit(char);

extern int compiling;
void lexerError(char*,int);

datum *lexInputString(char *str,int line)
{
  int i,j;
  int state,loc,dsLoc,finished;
  state = loc = dataLoc = dsLoc = finished = 0;

  for(i=0;i<MAX_DATA;++i)
  {
    for(j=0;j<32;++j)
      lexedData[i].str[j] = 0;

    lexedData[i].num = 0;
    lexedData[i].isValidNum = 0;
  }

  while(finished != 1)
  {
    #ifdef DEBUG
      fprintf(stderr,"Input lexer state %d loc %d str[loc] %d dataLoc %d dsLoc %d\n",state,loc,str[loc],dataLoc,dsLoc);
    #endif

    if(dataLoc >= MAX_DATA)
      exception("Maxmimum number of data items exceeded",line);

    if(state == 0) /* Starting to parse a new string */
    {
      if(str[loc] == '"')
      {
        loc++;
        lexedData[dataLoc].isValidNum = 0;
        state = 1;
      }
      else if (isLetter(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        loc++;
        dsLoc++;
        lexedData[dataLoc].isValidNum = 0;

        state = 3;
      }
      else if (str[loc] == '+' || str[loc] == '-')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        loc++;
        dsLoc++;
      
        state = 4;
      }
      else if (isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        loc++;
        dsLoc++;
   
        state = 5;
      }
      else if (str[loc] == '.')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        loc++;
        dsLoc++;

        state = 6;
      }
      else if (str[loc] == ' ')
        loc++;
      else
      {
        lexerError("State 0 unrecognized character",line);
        return 0;
      }
    }
    else if (state == 1) /* Parsing a quoted string */
    {
      if(isQuotedStringCharacter(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if (str[loc] == '"')
      {
        loc++;
        state = 2;
      }
      else if (str[loc] == 0)
      {
        lexerError("Missing ending quote",line);
        return 0;
      }
      else
      {
        lexerError("Unexpected character in a quoted string",line);
        return 0;
      }
    }
    else if (state == 2) /* Parsed a quoted string, waiting for , or end */
    {
      if(str[loc] == ',' || str[loc] == 0)
      {
        dataLoc++;
        dsLoc = 0;
        state = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;
      }
      else if(str[loc] == ' ')
      {
        loc++;
      }
      else
      {
        char msg[60];
        sprintf(msg,"Unexpected character %c while lexing input string",str[loc]);
        lexerError(msg,line);
        return 0;
      }
    }
    else if (state == 3) /* Parsing an unquoted string */
    {
      if(isUnquotedStringCharacter(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if (str[loc] == ',' || str[loc] == 0)
      {
        dsLoc--;
        while(lexedData[dataLoc].str[dsLoc] == ' ')
        {
          lexedData[dataLoc].str[dsLoc] = 0;
          dsLoc--;
        }
        dataLoc++;
        dsLoc = 0;
        state = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;
      }
      else
      {
        lexerError("Invalid character in unquoted string",line);
        return 0;
      }
    }
    else if (state == 4) /* Starting to parse a number, found the sign */
    {
      if(isDigit(str[loc]))
      { 
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;
    
        state = 5;
      }
      else if (str[loc] == '.')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;
  
        state = 6;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 5) /* Parsing a number, on the significand */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == '.')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 6;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == 'E')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 8;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == ',' || str[loc] == 0)
      {
        sscanf(lexedData[dataLoc].str,"%lf",&(lexedData[dataLoc].num));
        lexedData[dataLoc].isValidNum = 1;
        dataLoc++;
        dsLoc = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;

        state = 0;
      }
      else if(str[loc] == ' ')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }

        state = 11;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 6) /* Parsing a number, saw the . */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 7;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == 'E')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 8;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == ',' || str[loc] == 0)
      {
        sscanf(lexedData[dataLoc].str,"%lf",&(lexedData[dataLoc].num));
        lexedData[dataLoc].isValidNum = 1;
        dataLoc++;
        dsLoc = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;

        state = 0;
      }
      else if(str[loc] == ' ')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }

        state = 11;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 7) /* Parsing a number, saw a ., parsing the fractional part */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == 'E')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 8;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == ',' || str[loc] == 0)
      {
        sscanf(lexedData[dataLoc].str,"%lf",&(lexedData[dataLoc].num));
        lexedData[dataLoc].isValidNum = 1;
        dataLoc++;
        dsLoc = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;

        state = 0;
      }
      else if(str[loc] == ' ')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }

        state = 11;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 8) /* Parsing a number, saw an E */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 10;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == '+' || str[loc] == '-')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 9;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 9) /* Parsing a number, got a sign for the exrad */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        state = 10;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 10) /* Parsing a number, saw an E, parsing the exrad */
    {
      if(isDigit(str[loc]))
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }
      }
      else if(str[loc] == ',' || str[loc] == 0)
      {
        float fnum;
        sscanf(lexedData[dataLoc].str,"%E",&fnum);
        lexedData[dataLoc].num = fnum;
        lexedData[dataLoc].isValidNum = 1;
        dataLoc++;
        dsLoc = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;

        state = 0;
      }
      else if(str[loc] == ' ')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        dsLoc++;
        loc++;

        if(dsLoc >= 32)
        {
          lexerError("String size exceeded maximum length",line);
          return 0;
        }

        state = 11;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
    else if (state == 11) /* Parsing a number, saw a space */
    {
      if(str[loc] == ' ')
      {
        lexedData[dataLoc].str[dsLoc] = str[loc];
        loc++;
        dsLoc++;
      }
      else if(str[loc] == ',' || str[loc] == 0)
      {
        float fnum;

        dsLoc--;
        while(lexedData[dataLoc].str[dsLoc] == ' ')
        {
          lexedData[dataLoc].str[dsLoc] = 0;
          dsLoc--;
        }
        dsLoc++;

        sscanf(lexedData[dataLoc].str,"%E",&fnum);
        lexedData[dataLoc].num = fnum;
        lexedData[dataLoc].isValidNum = 1;
        dataLoc++;
        dsLoc = 0;
        if(str[loc] == ',')
          loc++;
        else
          finished = 1;
        state = 0;
      }
      else
      {
        lexedData[dataLoc].isValidNum = 0;
        state = 3;
      }
    }
  }

  return lexedData;
}

void lexerError(char *msg,int line)
{
  if(compiling)
  {
    fprintf(stderr, "Line %d: %s\n", line, msg);
    exit(1);
  }
  else
  {
    nonFatalError(msg,line);
  }
}

int isQuotedStringCharacter(char c)
{
  return c == '!'  || c == '#' || c == '$' || c == '%' || c == '&' ||
         c == '\'' || c == '(' || c == ')' || c == '*' || c == ',' ||
         c == '/'  || c == ':' || c == ';' || c == '<' || c == '=' ||
         c == '>'  || c == '?' || c == '^' || c == '_' ||
         isUnquotedStringCharacter(c);
}

int isPlainStringCharacter(char c)
{
  return c == '+' || c == '-' || c == '.' || isLetter(c) || isDigit(c);
}

int isUnquotedStringCharacter(char c)
{
  return c == ' ' || isPlainStringCharacter(c);
}

int isDigit(char c)
{
  return c >= '0' && c <= '9';
}

int isLetter(char c)
{
  return c >= 'A' && c <= 'Z';
}
