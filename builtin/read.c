/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "inputLexer.h"
#include "exception.h"

extern int numData;
extern datum storedData[];

int dataIndex = 0;

void assertDataLeft(int);

void restore()
{
  dataIndex = 0;
}

char *readString(int line)
{
  char *res;

  assertDataLeft(line);

  res = storedData[dataIndex].str;
  dataIndex++;

  return res;
}

double readNum(int line)
{
  double res;

  assertDataLeft(line);

  if(!storedData[dataIndex].isValidNum)
    exception("Cannot read string data into a numeric variable",line);

  res = storedData[dataIndex].num;
  dataIndex++;

  return res;
}

void assertDataLeft(int line)
{
  if(dataIndex >= numData)
    exception("No more data left to read",line);
}
