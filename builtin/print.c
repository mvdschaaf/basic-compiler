/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include <stdio.h>
#include "math.h"
#include "exception.h"
#include "print.h"

int cursorLoc = 1;

void printInteger(int);
void printDouble(double);
void printIntTab(int,int);
void trailingZerosToSpaces(char*);

void print(char *str)
{
   int i = 0;
   while(str[i] != 0) 
   {
     putchar(str[i]);
     i+=1;
   }

   cursorLoc += i;
}

void printNumber(double num)
{
  char numString[20];

  if(num > POS_INF || num < NEG_INF)
  {
    if(num > 0)
      print(" 9E99");
    else
      print("-9E99");
    return;
  }

  if(num > 1 || num <-1 || num == (int)num || num < .000001 || num > -.000001)
  {
    sprintf(numString,"% .7G ",num);
    print(numString);
  }
  else
  {
    if(num > 0)
      print(" .");
    else
    {
      print("-.");
      num = -num;
    }

    sprintf(numString,"%07d",(int)(num*10000000));
    trailingZerosToSpaces(numString);
    print(numString);
  }
}

void trailingZerosToSpaces(char *str)
{
  int i;
  i = 0;

  while(str[i] != 0)
    i++;

  i = i-1;

  while(str[i] == '0')
  {
    str[i] = ' ';
    i--;
  }
}

void printNewLine()
{
  putchar('\n');
  cursorLoc = 1;
}

void printTab(double newLoc, int lineNo)
{
  printIntTab((int)(newLoc+.5),lineNo);
}

void printIntTab(int newLoc, int lineNo)
{
  if(newLoc < 1)
  {
    nonFatalError("Tab value must not be less than 1",lineNo);
    newLoc = 0;
  }

  while(newLoc > 80)
    newLoc = newLoc % 80;

  while(cursorLoc < newLoc)
  {
    putchar(' ');
    cursorLoc += 1;
  }
}

void printComma()
{
  if(cursorLoc > 64)
  {
    printNewLine();
    return;
  }

  do
  {
    putchar(' ');
    cursorLoc += 1;
  } while ((cursorLoc-1) % 16 != 0);
}
