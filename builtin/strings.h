/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __STRINGS__H__
#define __STRINGS__H__

int stringComp(char*,char*);

#endif
