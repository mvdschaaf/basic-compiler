/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "math.h"
#include "exception.h"

double basicIntPow(double,int,int);
double overflowCheck(double,int);

int compiling = 0;

void randomize()
{
  srand(time(0));
}

double basicRnd(int line)
{
  return ((double)rand())/(((double)RAND_MAX)+1);
}

double basicTan(double x,int line)
{
  return tan(x);
}

double basicSin(double x,int line)
{
  return sin(x);
}

double basicCos(double x,int line)
{
  return cos(x);
}

double basicAtn(double x,int line)
{
  return atan(x);
}

double basicSqr(double x,int line)
{
  double guess;
  int i;

  if(x < 0)
    exception("Square root of a negative number",line);

  if(x == 0)
    return 0;

  guess = 10;

  if(x < 1)
  {
    for(i=0;i<128;++i)
      guess = guess-(guess*guess-x)/(2*guess);
  }
  else
  {
    while(basicAbs(guess*guess-x,line) > .00000001)
    {
      guess = guess-(guess*guess-x)/(2*guess);
    }
  }

  return guess;
}

double basicInt(double x,int line)
{
  if(x < 0 && x > -1)
    return -1;
  if(x < 0)
    return -basicInt(-x+.9999999,line);
  return (int)x;
}

double basicSgn(double x,int line)
{
  if(x > 0)
    return 1;
  if(x < 0)
    return -1;
  return 0;
}

double basicDiv(double x,double y,int line)
{
  if(y == 0)
  {
    nonFatalError("Division by zero",line); 
    if(x < 0)
      return NEG_INF;
    return POS_INF;
  }

  return x/y;
}

double basicAbs(double x,int line)
{
  if(x < 0)
    return -x;
  return x;
}

double basicExp(double x,int line)
{
  return overflowCheck(exp(x),line);
}

double basicLog(double x,int line)
{
  if(x == 0)
    exception("Log of zero",line);

  if(x < 0)
    exception("Log of a negative number",line);

  return log(x);
}

double basicPow(double x,double y,int line)
{
  if(x == 0 && y < 0)
  {
    nonFatalError("Zero to a negative exponent",line);
    return POS_INF;
  }
  if(x < 0 && y != (int)y)
  {
    exception("Negative number raised to a non-integral power",line);
  }
  if(y == (int)y && y < 10000)
  {
    return overflowCheck(basicIntPow(x,y,line),line);
  }
  return overflowCheck(basicExp(y*basicLog(x,line),line),line);
}

double basicIntPow(double x,int y,int line)
{
  int i,inv;
  double res = 1;
  inv = 0;

  if(y < 0)
  {
    inv = 1;
    y = -y;
  }
  
  for(i=0;i<y;++i)
    res *= x;

  if(inv)
    return 1/res;
  return res;
}

double overflowCheck(double num,int line)
{
  if(num >= POS_INF)
  {
    nonFatalError("Overflow",line);
    return POS_INF;
  }
  if(num <= NEG_INF)
  {
    nonFatalError("Overflow",line);
    return NEG_INF;
  }
  return num;
}
