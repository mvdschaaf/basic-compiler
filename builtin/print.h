/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___PRINT_H___
#define ___PRINT_H___

void print(char*);
void printNumber(double);
void printNewLine();
void printTab(double,int);
void printIntTab(int,int);
void printComma();

#endif
