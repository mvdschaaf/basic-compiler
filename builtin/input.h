/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __INPUT_H___
#define __INPUT_H___

#include "inputLexer.h"

datum *basicInput(int,int,int);

#endif
