/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include <stdio.h>
#include <stdlib.h>

void nonFatalError(char *message, int lineNo)
{
  printf("Error line %d: %s\n",lineNo,message);
}

void exception(char *message, int lineNo)
{
  printf("Error line %d: %s\n",lineNo,message);
  exit(1);
}
