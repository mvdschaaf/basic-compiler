/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___ARRAYS_H__
#define ___ARRAYS_H__


void setArraySize(char,int,int,int);
void assignValue(char,double,double,double,int);
double retrieveValue(char,double,double,int);

#endif
