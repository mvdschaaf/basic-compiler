/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___MATH__H__
#define ___MATH__H__

static const double POS_INF =  10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0;
static const double NEG_INF = -10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0;

double basicDiv(double,double,int);
double basicAbs(double,int);
double basicExp(double,int);
double basicLog(double,int);
double basicPow(double,double,int);
double basicSgn(double,int);

#endif
