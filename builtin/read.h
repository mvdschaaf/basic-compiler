/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___READ__H___
#define ___READ__H___

void restore();
char *readString(int);
double readNum(int);

#endif
