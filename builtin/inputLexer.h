/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __INPUT__LEXER___
#define __INPUT__LEXER___

typedef struct d {
  char str[32];
  double num;
  int isValidNum;
} datum;

datum *lexInputString(char*,int);

#endif
