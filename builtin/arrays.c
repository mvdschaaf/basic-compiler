/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "exception.h"

#define MAX_ARRAY_SIZE 1280

double arrayStore[26][MAX_ARRAY_SIZE][MAX_ARRAY_SIZE];

int arrayDims[26][2] = { {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10}, {10,10},
                         {10,10}, {10,10}, {10,10} };

int optionValue = 0;

int roundDoub(double);

void setOptionBase(int base)
{
  optionValue = base;
}

void setArraySize(char array,int rowSize,int colSize,int line)
{
  arrayDims[array-'A'][0] = rowSize;
  arrayDims[array-'A'][1] = colSize;
}

void assignValue(char array,double row,double col,double value,int line)
{
  int intRow,intCol;

  intRow = roundDoub(row);
  intCol = roundDoub(col);

  if(intRow > arrayDims[array-'A'][0] || intCol > arrayDims[array-'A'][1])
    exception("Dimension error: too large",line);
  if(intRow < optionValue ||  intCol < optionValue)
    exception("Dimension error: too small",line);

  arrayStore[array-'A'][intRow][intCol] = value;
}

double retrieveValue(char array,double row,double col,int line)
{
  int intRow,intCol;

  intRow = roundDoub(row);
  intCol = roundDoub(col);

  if(intRow > arrayDims[array-'A'][0] || intCol > arrayDims[array-'A'][1])
    exception("Dimension error: too large",line);
  if(intRow < optionValue || intCol < optionValue)
    exception("Dimension error: too small",line);

  return arrayStore[array-'A'][intRow][intCol];
}

int roundDoub(double n)
{
  if(n > -.5)
    return (int)(n+.5);

  return -1;
}
