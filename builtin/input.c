/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include <stdio.h>
#include "inputLexer.h"
#include "exception.h"

extern int dataLoc;

datum *basicInput(int types,int numToRead,int line)
{
  int i,hasError;
  datum *inputStorage;
  char str[80];
  int typeList[16];

  if(numToRead > 16)
    exception("Number of variables to input is more than allowed",line);

  for(i=numToRead-1;i>=0;--i)
  {
    typeList[i] = types%2;
    types >>= 1;
  }

  hasError = 1;

  while(hasError)
  {
    hasError = 0;

    printf("? ");
    fgets(str,80,stdin);
    i=0;
    while(str[i] != '\n' && i < 80)
      ++i;

    if(i == 80)
    {
      printf("Response exceeded buffer size\n");
      hasError = 1;
      while(fgetc(stdin)!='\n');
      continue;
    }
   
    str[i] = 0;
    inputStorage = lexInputString(str,line);

    if(inputStorage == 0)
    {
      hasError = 1;
      continue;
    }

    if(dataLoc > numToRead)
    {
      printf("Too many data items inputted\n");
      hasError = 1;
      continue;
    }
    else if(dataLoc < numToRead)
    {
      printf("Too few data items inputted\n");
      hasError = 1;
      continue;
    }

    for(i=0;i<numToRead;++i)
    {
      if(typeList[i] == 0 && !inputStorage[i].isValidNum)
      {
        printf("Found string but expected number in input argument %d\n",i);
        hasError = 1;
      }
    }
  }

  return inputStorage;
}
