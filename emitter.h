/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __EMITTER__H__
#define __EMITTER__H__

#include "parser.h"
#include "builtin/inputLexer.h"

void emitSetup();
void emitPrint();
void emitPrintingSemicolon();
void emitPrintingComma();
void emitTab();
void emitLineNumber(int num);
void emitStop();
void emitStringLetConstant(char,char*);
void emitStringLetVariable(char,char);
void emitPrintStringVar(int);
void emitPrintStringConst(char*);
void emitRandomize();

void emitAddition(int,int);
void emitSubtraction(int,int);
void emitMultiplication(int,int);
void emitDivision(int,int);
void emitPower(int,int);

void emitNumericRead(char*);
void emitStringRead(char*);
void emitRestore();
void emitData(datum*,int);
void emitArrayRead(char*,int,int);

void emitInput(int,int);
void emitInputLetNum(char*,int,int);
void emitInputLetStr(char*,int,int);
void emitInputLetArray(char*,int,int,int,int);

void emitArrayLet(char*,int,int);
void emitRetrieveArray(char*,int,int);
void emitDim(char*,int,int);
void emitOptionBase(int);

void emitOnGoto(int,int[],int);

void emitDefaultStep();
void emitNext(int,char*,int);
void emitForLoopStart(int,char*,int,int,int);

void emitCallFunction(char*,int[],int);
void emitDefinedFunctions(FuncDec*,int);

void emitUserDefFunStart(char,char*,int);
void emitUserDefFunEnd(char*);

void emitNumericVar(char*);
void emitFunctionVar(char*);
void emitNumericRep(double);
void emitNumericLet(char*);
void emitPrintExpression();
void emitNegate();
void emitGoto(int);
void emitGosub(int,int);
void emitReturn();
void emitDone(short[],int,int);
void emitNewline();
void emitStringVarVarComparison(char*,char*,int,int);
void emitStringVarConstComparison(char*,char*,int,int);
void emitNumericComparison(int,int,int,int);

#endif
