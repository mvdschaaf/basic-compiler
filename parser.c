/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"
#include "lexer.h"
#include "error.h"
#include "emitter.h"
#include "parser.h"

#define MAX_FOR_NEST    32
#define MAX_FOR_BLOCKS 128
#define MAX_GOTOS      256

typedef struct Node {
  int operation;
  struct Node **children;
  int numChildren;

  double num;
  char *varName;
} Node;

FuncDec builtinFuncs[] = {
   {"ABS","basicAbs",1,0},
   {"INT","basicInt",1,0},
   {"SGN","basicSgn",1,0},
   {"SQR","basicSqr",1,0},
   {"ATN","basicAtn",1,0},
   {"COS","basicCos",1,0},
   {"EXP","basicExp",1,0},
   {"LOG","basicLog",1,0},
   {"SIN","basicSin",1,0},
   {"TAN","basicTan",1,0},
   {"RND","basicRnd",0,0},
   {0    ,0         ,0,0}
};

FuncDec functions[50];
int lastFuncEntry = 0;
int compiling = 1;

Node *userDefFns[26];
char *userDefFnVars[26];

int lookAhead;
int sawEnd;
int seenArray;
int forLoopNum;
int alreadyOptioned;
int optionBase = 0;

extern int dataLoc;

short lineNumbers[10000];
short lnIndex = 0;

short futureNumbers[100][3];
short fnBack = 0;

char forLoopVars[MAX_FOR_NEST][2];
int flvIndex;

int forLoopBlocks[MAX_FOR_BLOCKS][2];
int flbIndex;

int gotoDests[MAX_GOTOS][2];
int gdIndex;

short arraySize[26];
int arrayDim[26][2];

void match(int t);
void printType(int);

void line_number();
void block();
void forBlock();
void line();

void statement();
void printStatement();
void endStatement();
void stopStatement();
void letStatement();
void remarkStatement();
void goStatement();
void gotoStatement();
void gosubStatement();
void returnStatement();
void optionStatement();
void inputStatement();
void randomizeStatement();
void defStatement();
void readStatement();
void restoreStatement();
void dataStatement();
void dimStatement();
void onStatement();

void printSeparator();
void printList();
void printItem();

void arrayDeclaration();

void relationalExpression();
void numericRelationalExpression();
void stringRelationalExpression();

void numericExpression();

void stringLetStatement();
void numericLetStatement();

Node *createNumExpSyntaxTree();
void emitSyntaxTree(Node*);
void freeSyntaxTree(Node*);

Node *term();
Node *factor();
Node *primary();
Node *numericFunction();

Node *makeNode(int,Node*,Node*);
Node *makeNumLeaf(double);
Node *makeVarLeaf(char*);

void changeVarToFnVar(Node*,char*);

void initParser();
void initBuiltinFunctions();
void emitUserDefinedFunctions();
void addFunction(char *,char *,int,int);

void assertStringSize(char*);
void assertStringVar(char*);
void assertNumericVar(char*);
void assertArrayVar(char*);
void assertInteger(double);
void assertFutureLineNums(int);
void assertLineNumExists(int);
void assertForLoopUnique(char*);
void assertNoJumpsIntoForLoops();
void assertConsistentArraySize(char,int);
int assertFunctionDefined(char*);
void assertUserDefFunctionName(char*);
void assertFunctionNotDeclared(char*);

int isStringVar(char*);
int isNumericVar(char*);
int isInteger(double);
int isFunctionName(char*);

void parse()
{
  initParser();
  initBuiltinFunctions();
  emitSetup();

  lookAhead = lexan();

  block();

  if(lookAhead != DONE && sawEnd == 1)
  {
    fprintf(stderr,"Unexpected %d\n",lookAhead);
    error("Found more statements after END statement");
  }
  else if(lookAhead == NEXT)
    error("Missing corresponding FOR");

  assertNoJumpsIntoForLoops();

  emitDone(lineNumbers,lnIndex,forLoopNum);

  emitDefinedFunctions(functions,lastFuncEntry);
  emitUserDefinedFunctions();
}

void initParser()
{
  int i;

  sawEnd = seenArray = alreadyOptioned = 0;
  forLoopNum = 1;
  flvIndex = gdIndex = flbIndex = 0;

  for(i=0;i<26;++i)
  {
    arraySize[i] = 0;
    userDefFns[i] = 0;
    arrayDim[i][0] = arrayDim[i][1] = -1;
  }
}

void initBuiltinFunctions()
{
  FuncDec *fd;
  for (fd = builtinFuncs; fd->basicName; fd++)
    addFunction(fd->basicName,fd->asmName,fd->numArgs,fd->declaredLine);
}

void emitUserDefinedFunctions()
{
 int i;

 for(i=0;i<26;++i)
  {
    if(userDefFns[i] != 0)
    {
      int fdl;
      char name[4];

      name[0] = 'F'; name[1] = 'N'; name[3] = 0;
      name[2] = 'A'+i;

      fdl = assertFunctionDefined(name);

      emitUserDefFunStart('A'+i,userDefFnVars[i],functions[fdl].numArgs);

      emitSyntaxTree(userDefFns[i]);
      emitUserDefFunEnd(userDefFnVars['A'+i]);
    }
  }
}

void block()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"block\n");
  #endif

  while(lookAhead != DONE && lookAhead != NEXT && sawEnd != 1)
  {
    line();
  }
}

void line()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"line\n");
  #endif
  line_number();
  if(lookAhead != NEXT)
  {
    statement();
    match('\n');
    emitNewline();
  }
}

void line_number()
{
  int intVal;

  assertInteger(numTokenVal);
  intVal = (int)numTokenVal;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"line number %d\n",intVal);
  #endif

  if(intVal > 9999)
    error("line number too large");

  if(lineNumber >= intVal)
  {
    lineNumber = intVal;
    error("line number should be larger than the last one");
  }

  lineNumber = intVal;
  lineNumbers[lnIndex] = lineNumber;
  lnIndex++;

  assertFutureLineNums(lineNumber);

  emitLineNumber(intVal);

  match(NUM);
}

void statement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"statement\n");
  #endif
  if(lookAhead == PRINT)
    printStatement();
  else if (lookAhead == END)
    endStatement();
  else if (lookAhead == STOP)
    stopStatement();
  else if (lookAhead == LET)
    letStatement();
  else if (lookAhead == REM)
    remarkStatement();
  else if (lookAhead == GO)
    goStatement();
  else if (lookAhead == GOTO)
    gotoStatement();
  else if (lookAhead == GOSUB)
    gosubStatement();
  else if (lookAhead == RETURN)
    returnStatement();
  else if (lookAhead == IF)
    relationalExpression();
  else if (lookAhead == FOR)
    forBlock();
  else if (lookAhead == NEXT)
    return;
  else if (lookAhead == DIM)
    dimStatement();
  else if (lookAhead == OPTION)
    optionStatement();
  else if (lookAhead == ON)
    onStatement();
  else if (lookAhead == READ)
    readStatement();
  else if (lookAhead == RESTORE)
    restoreStatement();
  else if (lookAhead == DATA)
    dataStatement();
  else if (lookAhead == INPUT)
    inputStatement();
  else if (lookAhead == RANDOMIZE)
    randomizeStatement();
  else if (lookAhead == DEF)
    defStatement();
  else
    error("Unknown statement");
}

void defStatement()
{
  char asmName[16];
  char *fnName;
  char *varName;

  varName = 0;

  match(DEF);

  fnName = symTable[(int)numTokenVal].lexPtr;

  assertUserDefFunctionName(fnName);
  assertFunctionNotDeclared(fnName);

  match(VAR);

  if(lookAhead == '(')
  {
    match('(');

    varName = symTable[(int)numTokenVal].lexPtr;

    assertNumericVar(varName);
    match(VAR);

    match(')');
  }

  match(EQUALS);

  userDefFns[fnName[2]-'A'] = createNumExpSyntaxTree();

  sprintf(asmName,"FN%c",fnName[2]);

  if(varName != 0)
  {
    addFunction(fnName,asmName,1,lineNumber);
    changeVarToFnVar(userDefFns[fnName[2]-'A'],varName);
    userDefFnVars[fnName[2]-'A'] = varName;
  }
  else
    addFunction(fnName,asmName,0,lineNumber);
}

void changeVarToFnVar(Node *head,char *varName)
{
  int i;

  if(head->operation == VAR && varName[0] == head->varName[0] && varName[1] == head->varName[1])
    head->operation = FNVAR;

  for(i=0;i<head->numChildren;++i)
    changeVarToFnVar(head->children[i],varName);
}

void randomizeStatement()
{
  match(RANDOMIZE);
  emitRandomize();
}

void inputStatement()
{
  char varNames[16][4];
  Node *arrayVars[16][2];
  int vnIndex,typeInt,i,inputtedData;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"inputStatement\n");
  #endif

  for(i=0;i<16;++i)
    arrayVars[i][0] = arrayVars[i][1] = 0;

  vnIndex = typeInt = 0;

  match(INPUT);

  if(lookAhead != VAR)
    error("Expected variable");

  strcpy(varNames[vnIndex],symTable[(int)numTokenVal].lexPtr);
  if(isStringVar(varNames[vnIndex]))
    typeInt |= 1;
  typeInt <<= 1;

  match(VAR);

  if(lookAhead == '(')
  {
    assertArrayVar(varNames[vnIndex]);
    match('(');

    arrayVars[vnIndex][0] = createNumExpSyntaxTree();

    if(lookAhead == ',')
    {
      match(',');

      arrayVars[vnIndex][1] = createNumExpSyntaxTree();
    }
    else
    {
      arrayVars[vnIndex][1] = makeNumLeaf(optionBase);
    }

    match(')');
  }

  vnIndex++;

  while(lookAhead == ',')
  {
    match(',');

    if(lookAhead != VAR)
      error("Expected variable");

    strcpy(varNames[vnIndex],symTable[(int)numTokenVal].lexPtr);
    if(isStringVar(varNames[vnIndex]))
      typeInt |= 1;
    typeInt <<= 1;

    match(VAR);

    if(lookAhead == '(')
    {
      assertArrayVar(varNames[vnIndex]);
      match('(');

      arrayVars[vnIndex][0] = createNumExpSyntaxTree();

      if(lookAhead == ',')
      {
        match(',');

        arrayVars[vnIndex][1] = createNumExpSyntaxTree();
      }
      else
      {
        arrayVars[vnIndex][1] = makeNumLeaf(optionBase);
      }

      match(')');
    }

    vnIndex++;
  }

  typeInt >>= 1;

  emitInput(typeInt,vnIndex);
  inputtedData = tempVarNum-1;

  for(i=0;i<vnIndex;++i)
  {
    if(isStringVar(varNames[i]))
    {
      emitInputLetStr(varNames[i],inputtedData,i);
    } 
    else if(arrayVars[i][0] != 0)
    {
      int row,col;

      emitSyntaxTree(arrayVars[i][0]);
      row = tempVarNum-1;
      emitSyntaxTree(arrayVars[i][1]);
      col = tempVarNum-1;

      emitInputLetArray(varNames[i],row,col,inputtedData,i);

      freeSyntaxTree(arrayVars[i][0]);
      freeSyntaxTree(arrayVars[i][1]);
    }
    else if(isNumericVar(varNames[i]))
    {
      emitInputLetNum(varNames[i],inputtedData,i);
    }
  }
}

void readStatement()
{
  char *varName;
  #ifdef DEBUG_PARSE
    fprintf(stderr,"readStatement\n");
  #endif

  match(READ);
  varName = symTable[(int)numTokenVal].lexPtr; 
  match(VAR);

  if(isStringVar(varName))
    emitStringRead(varName);
  else if (isNumericVar(varName) && lookAhead != '(')
    emitNumericRead(varName);
  else
  {
    int arrayVar1,arrayVar2;

    assertArrayVar(varName);

    seenArray = 1;

    match('(');

    numericExpression();
    arrayVar1 = tempVarNum-1;

    if(lookAhead == ',')
    {
      assertConsistentArraySize(varName[0],2);
      match(',');

      numericExpression();
      arrayVar2 = tempVarNum-1;

      match(')');

      emitArrayRead(varName,arrayVar1,arrayVar2);
    }
    else
    {
      assertConsistentArraySize(varName[0],1);
      match(')');

      emitNumericRep(optionBase);
      arrayVar2 = tempVarNum-1;

      emitArrayRead(varName,arrayVar1,arrayVar2);
    }
  }

  while(lookAhead == ',')
  {
    match(',');

    varName = symTable[(int)numTokenVal].lexPtr; 
    match(VAR);

    if(isStringVar(varName))
      emitStringRead(varName);
    else if (isNumericVar(varName) && lookAhead != '(')
      emitNumericRead(varName);
    else
    {
      int arrayVar1,arrayVar2;

      assertArrayVar(varName);

      seenArray = 1;

      match('(');

      numericExpression();
      arrayVar1 = tempVarNum-1;

      if(lookAhead == ',')
      {
        assertConsistentArraySize(varName[0],2);
        match(',');

        numericExpression();
        arrayVar2 = tempVarNum-1;

        match(')');

        emitArrayRead(varName,arrayVar1,arrayVar2);
      }
      else
      {
        assertConsistentArraySize(varName[0],1);
        match(')');

        emitNumericRep(optionBase);
        arrayVar2 = tempVarNum-1;

        emitArrayRead(varName,arrayVar1,arrayVar2);
      }
    }
  }
}

void restoreStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"restoreStatement\n");
  #endif

  match(RESTORE);
  emitRestore();
}

void dataStatement()
{
  char *dataStr;

  datum *data;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"dataStatement\n");
  #endif

  dataStr = lexanData();
  data = lexInputString(dataStr,lineNumber);
  if(data == 0)
    exit(1);
  
  lookAhead = lexan();

  emitData(data,dataLoc);
}

void onStatement()
{
  int conditionVar;
  int dests[16];
  int destIndex;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"onStatement\n");
  #endif
  match(ON);

  numericExpression();
  conditionVar = tempVarNum-1;

  if(lookAhead == GO)
  {
    match(GO);
    match(TO);
  }
  else
    match(GOTO);

  destIndex = 0;
  assertInteger(numTokenVal);
  dests[destIndex] = (int)numTokenVal;
  match(NUM);

  assertLineNumExists(dests[destIndex]);
  destIndex++;

  while(lookAhead != '\n')
  {
    if(destIndex > 15)
      error("Too many destinations in ON GOTO");

    match(',');

    assertInteger(numTokenVal);
    dests[destIndex] = (int)numTokenVal;
    match(NUM);

    assertLineNumExists(dests[destIndex]);
    destIndex++;
  }

  emitOnGoto(conditionVar,dests,destIndex);
}

void optionStatement()
{
  int base;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"optionStatement\n");
  #endif

  match(OPTION);
  match(BASE);

  if(seenArray)
    error("Option statement should come before any array statements");

  if(alreadyOptioned)
    error("Only one option base statement allowed");

  if(lookAhead != NUM)
    error("Option base must have a number");

  if(numTokenVal != 0 && numTokenVal != 1)
    error("Option base can only be 0 or 1");

  base = (int)numTokenVal;
  match(NUM); 

  emitOptionBase(base);

  optionBase = base;
  alreadyOptioned = 1;
}

void dimStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"dimStatement\n");
  #endif
  match(DIM);

  seenArray = 1;

  arrayDeclaration();

  while(lookAhead == ',')
  {
    match(',');
    arrayDeclaration();
  }
}

void arrayDeclaration()
{
  char *arrayName;
  int rowSize,colSize;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"arrayDeclaration\n");
  #endif

  rowSize = colSize = optionBase;

  arrayName = symTable[(int)numTokenVal].lexPtr; 
  assertArrayVar(arrayName);

  if(arrayDim[arrayName[0]-'A'][0] > -1)
    error("Variable dimensioned twice");
  if(arraySize[arrayName[0]-'A'] != 0)
    error("Array dimensioned after it has been used");

  match(VAR);
  match('(');

  if(!isInteger(numTokenVal))
    error("Only integer sizes allowed");

  rowSize = (int)numTokenVal;
  match(NUM);

  if(lookAhead == ',')
  {
    match(',');
    assertConsistentArraySize(arrayName[0],2);
    if(!isInteger(numTokenVal))
      error("Only integer sizes allowed");

    colSize = (int)numTokenVal;
    match(NUM);
  }
  else
  {
    assertConsistentArraySize(arrayName[0],1);
  }

  if(rowSize > 1280 || colSize > 1280)
    error("Maximum array size is 1280");

  if(rowSize < optionBase || colSize < optionBase)
    error("Array size is too small");

  arrayDim[arrayName[0]-'A'][0] = rowSize;
  arrayDim[arrayName[0]-'A'][1] = colSize;
  emitDim(arrayName,rowSize,colSize);
  match(')');
}

void forBlock()
{
  char *controlVar,*nextControlVar;
  int loopNum,initialValueVarNum,stepVarNum,limitVarNum,forLineNum,flbNum;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"for-block\n");
  #endif

  loopNum = forLoopNum;
  forLineNum = lineNumber;
  flbNum = flbIndex;

  forLoopBlocks[flbNum][0] = lineNumber;
  flbIndex++;

  match(FOR);

  controlVar = symTable[(int)numTokenVal].lexPtr;
  assertNumericVar(controlVar);
  assertForLoopUnique(controlVar);

  match(VAR);

  forLoopVars[flvIndex][0] = controlVar[0];
  forLoopVars[flvIndex][1] = controlVar[1];

  flvIndex++;

  if(flvIndex > MAX_FOR_NEST)
    error("Too many for loops");

  match(EQUALS);

  numericExpression();
  initialValueVarNum = tempVarNum-1;

  match(TO);

  numericExpression();
  limitVarNum = tempVarNum-1;

  if(lookAhead == STEP)
  {
    match(STEP);
   
    numericExpression();
    stepVarNum = tempVarNum-1;
  }
  else
  {
    emitDefaultStep();
    stepVarNum = tempVarNum-1;
  }

  emitForLoopStart(loopNum,controlVar,initialValueVarNum,limitVarNum,stepVarNum);
  forLoopNum+=1;

  match('\n');
  emitNewline();

  block();

  if(lookAhead != NEXT)
  {
    char msg[60];
    sprintf(msg,"Missing NEXT for FOR on line %d",forLineNum);
    error(msg);
  }

  forLoopBlocks[flbNum][1] = lineNumber;

  match(NEXT);
  nextControlVar = symTable[(int)numTokenVal].lexPtr;
  assertNumericVar(nextControlVar);
  match(VAR);

  if(controlVar[0] != nextControlVar[0] || controlVar[1] != nextControlVar[1])
  {
    char msg[100];
    sprintf(msg,"NEXT should match corresponding FOR on line %d",forLineNum);
    error(msg);
  }

  flvIndex--;

  emitNext(loopNum,controlVar,stepVarNum);
}

void relationalExpression()
{
  char *varName;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"relationalExpression\n");
  #endif

  match(IF);

  if(lookAhead == VAR)
    varName = symTable[(int)numTokenVal].lexPtr;

  if(lookAhead == '-' || lookAhead == '+' || lookAhead == '(' || lookAhead == NUM || (lookAhead == VAR && isNumericVar(varName)) || (lookAhead == VAR && isFunctionName(varName)))
    numericRelationalExpression();
  else if (lookAhead == STR || (lookAhead == VAR && isStringVar(varName)))
    stringRelationalExpression();
  else
    error("Expected string, number, string variable or numeric variable");
}

void numericRelationalExpression()
{
  int num1,num2,op,destLine;

  numericExpression();
  num1 = tempVarNum-1;

  if(lookAhead != EQUALS && lookAhead != NOTEQUALS && lookAhead != LESSTHAN &&
     lookAhead != GREATERTHAN && lookAhead != NOTLESS && lookAhead != NOTGREATER)
  {
    char msg[80];
    sprintf(msg,"Unknown numeric comparison operator %d",lookAhead);
    error(msg);
  }
  op = lookAhead;

  match(op);

  numericExpression();

  num2 = tempVarNum-1;

  match(THEN);
  
  destLine = (int)numTokenVal;

  match(NUM);

  gotoDests[gdIndex][0] = destLine;
  gotoDests[gdIndex][1] = lineNumber;
  gdIndex++;

  assertLineNumExists(destLine);
  emitNumericComparison(num1,num2,op,destLine);
}

void stringRelationalExpression()
{
  char *var1,*var2,*str1;
  int op,destLine;

  if(lookAhead == VAR)
  {
    var1 = symTable[(int)numTokenVal].lexPtr;
    assertStringVar(var1);

    match(VAR);
    if(lookAhead != EQUALS && lookAhead != NOTEQUALS)
      error("Invalid string comparison operator");

    op = lookAhead;
    match(op);
 
    if(lookAhead == VAR)
    {
      var2 = symTable[(int)numTokenVal].lexPtr;
      assertStringVar(var2);

      match(VAR);
      match(THEN);
      destLine = (int)numTokenVal;
      match(NUM);

      gotoDests[gdIndex][0] = destLine;
      gotoDests[gdIndex][1] = lineNumber;
      gdIndex++;

      assertLineNumExists(destLine);
      emitStringVarVarComparison(var1,var2,op,destLine);
    }
    else if (lookAhead == STR)
    {
      str1 = strTokenVal;
      
      match(STR);
      match(THEN);
      destLine = (int)numTokenVal;
      match(NUM);

      gotoDests[gdIndex][0] = destLine;
      gotoDests[gdIndex][1] = lineNumber;
      gdIndex++;

      assertLineNumExists(destLine);
      emitStringVarConstComparison(var1,str1,op,destLine);
    }
    else
      error("Expected variable or string constant");
  }
  else if (lookAhead == STR)
  {
    str1 = strTokenVal;
 
    match(STR);
    if(lookAhead != EQUALS && lookAhead != NOTEQUALS)
      error("Invalid string comparison operator");

    op = lookAhead;
    match(op);

    if(lookAhead == VAR)
    {
      var1 = symTable[(int)numTokenVal].lexPtr;
      assertStringVar(var1);

      match(VAR);
      match(THEN);
      destLine = (int)numTokenVal;
      match(NUM);

      gotoDests[gdIndex][0] = destLine;
      gotoDests[gdIndex][1] = lineNumber;
      gdIndex++;

      assertLineNumExists(destLine);
      emitStringVarConstComparison(var1,str1,op,destLine);
    }
    else if (lookAhead == STR)
    {
      error("Do it yourself :/");
    }
    else
      error("Expected variable or string constant");
  }
  else
    error("Expected variable or string constant");
}

void returnStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"returnStatement\n");
  #endif
  match(RETURN);

  emitReturn();
}

void goStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"goStatement\n");
  #endif

  match(GO);

  if(lookAhead == TO)
    gotoStatement();
  else if (lookAhead == SUB)
    gosubStatement();
  else
    error("Expected TO or SUB");
}

void gotoStatement()
{
  int destination;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"gotoStatement\n");
  #endif

  if(lookAhead == GOTO)
  {
    match(GOTO);
  }
  else
  {
    match(TO);
  }

  destination = (int)numTokenVal;
  match(NUM);

  gotoDests[gdIndex][0] = destination;
  gotoDests[gdIndex][1] = lineNumber;
  gdIndex++;

  if(gdIndex > MAX_GOTOS)
    error("Exceeded maximum amount of GOTOs");

  assertLineNumExists(destination);
  emitGoto(destination);
}

void gosubStatement()
{
  int src,dest;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"gosubStatement\n");
  #endif

  if(lookAhead == GOSUB)
  {
    match(GOSUB);
  }
  else
  {
    match(SUB);
  }

  src = lineNumber;
  dest = (int)numTokenVal;

  match(NUM);

  assertLineNumExists(dest);
  emitGosub(src,dest);
}



void remarkStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"remarkStatement\n");
  #endif
  lexanRem();
  lookAhead = lexan();
}

void letStatement()
{
  char *varName;
  #ifdef DEBUG_PARSE
    fprintf(stderr,"letStatement\n");
  #endif

  match(LET);

  varName = symTable[(int)numTokenVal].lexPtr;

  if(isStringVar(varName))
    stringLetStatement();
  else if(isNumericVar(varName))
    numericLetStatement(); 
  else
    error("Invalid variable name");
}

void stringLetStatement()
{
  char *strVar;
  char *constStr;
  char *srcStrVar;
  #ifdef DEBUG_PARSE
    fprintf(stderr,"stringLetStatement\n");
  #endif

  strVar = symTable[(int)numTokenVal].lexPtr;
  assertStringVar(strVar);
  match(VAR);

  match(EQUALS);

  if(lookAhead == STR)
  {
    constStr = strTokenVal;

    assertStringSize(constStr);

    match(STR);
    emitStringLetConstant(strVar[0],constStr);
  }
  else if (lookAhead == VAR)
  {
    srcStrVar = symTable[(int)numTokenVal].lexPtr;
    assertStringVar(srcStrVar);
    match(VAR);
    emitStringLetVariable(strVar[0],srcStrVar[0]);
  }
  else if (lookAhead == NUM)
  {
    error("Cannot assign a number to a string");
  }
  else
  {
    error("Expected string or string variable");
  }
}

void numericLetStatement()
{
  char *numVar = symTable[(int)numTokenVal].lexPtr;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"numericLetStatement\n");
  #endif

  assertNumericVar(numVar);

  match(VAR);

  if(lookAhead == '(')
  {
    int arrayVar1,arrayVar2;

    assertArrayVar(numVar);

    seenArray = 1;

    match('(');

    numericExpression();
    arrayVar1 = tempVarNum-1;

    if(lookAhead == ',')
    {
      assertConsistentArraySize(numVar[0],2);
      match(',');

      numericExpression();
      arrayVar2 = tempVarNum-1;

      match(')');

      match(EQUALS);
      numericExpression();
      emitArrayLet(numVar,arrayVar1,arrayVar2);
    }
    else
    {
      assertConsistentArraySize(numVar[0],1);
      match(')');

      emitNumericRep(optionBase);
      arrayVar2 = tempVarNum-1;

      match(EQUALS);
      numericExpression();

      emitArrayLet(numVar,arrayVar1,arrayVar2);
    }
  }
  else
  {
    match(EQUALS);

    numericExpression();

    emitNumericLet(numVar);
  }
}

void numericExpression()
{
  Node *exp;

  exp = createNumExpSyntaxTree();
  emitSyntaxTree(exp);
  freeSyntaxTree(exp);
}

void emitSyntaxTree(Node *head)
{
  int op = head->operation;
  if(op == '+' || op == '-' || op == '*' || op == '/' || op == '^')
  {
    int left,right;

    emitSyntaxTree(head->children[0]);
    left = tempVarNum-1;
    emitSyntaxTree(head->children[1]);
    right = tempVarNum-1;

    if(op == '+')
      emitAddition(left,right);
    else if(op == '-')
      emitSubtraction(left,right);
    else if(op == '*')
      emitMultiplication(left,right);
    else if(op == '/')
      emitDivision(left,right);
    else if(op == '^')
      emitPower(left,right);
  }
  else if(op == NUM)
  {
    emitNumericRep(head->num);
  }
  else if(op == VAR)
  {
    emitNumericVar(head->varName);
  }
  else if(op == FNVAR)
  {
    emitFunctionVar(head->varName);
  }
  else if(op == ARRAY)
  {
    int row,col;
    char *varName;

    varName = head->children[0]->varName;

    emitSyntaxTree(head->children[1]);
    row = tempVarNum-1;
    emitSyntaxTree(head->children[2]);
    col = tempVarNum-1;
    
    emitRetrieveArray(varName,row,col);
  }
  else if(op == FUNC)
  {
    int *tempVars;
    int numArgs,i;

    numArgs = head->numChildren-1;

    tempVars = malloc(numArgs*sizeof(int));

    for(i=0;i<numArgs;++i)
    {
      emitSyntaxTree(head->children[i+1]);
      tempVars[i] = tempVarNum-1;
    }

    emitCallFunction(head->children[0]->varName,tempVars,numArgs);
  }
}

void freeSyntaxTree(Node *head)
{
  int i;

  if(head->numChildren > 0)
  {
    for(i=0;i<head->numChildren;++i)
      freeSyntaxTree(head->children[i]);
  
    free(head->children);
  }

  free(head);
}

Node *createNumExpSyntaxTree()
{
  Node *res;
  int negate = 0;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"createNumExpSyntaxTree");
  #endif

  if(lookAhead == '-')
  {
    negate = 1;
    match('-');
  }
  else if (lookAhead == '+')
    match('+');

  res = term();
  if(negate)
  {
    Node *zero;

    zero = makeNumLeaf(0);

    res = makeNode('-',zero,res);
  }

  while(lookAhead == '-' || lookAhead == '+')
  {
    int op;
    op = lookAhead;

    match(op);
    res = makeNode(op,res,term());
  }

  return res;
}

Node *term()
{
  Node *res;
  #ifdef DEBUG_PARSE
    fprintf(stderr,"term\n");
  #endif

  res = factor();
  while(lookAhead == '*' || lookAhead == '/')
  {
    int op;
    op = lookAhead;

    match(op);
    res = makeNode(op,res,factor());
  }

  return res;
}
  
Node *factor()
{
  Node *res;
  #ifdef DEBUG_PARSE
    fprintf(stderr,"factor\n");
  #endif

  res = primary();
  while(lookAhead == '^')
  {
    match('^');
    res = makeNode('^',res,primary());
  }

  return res;
}

Node *primary()
{
  Node *res;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"primary\n");
  #endif

  if(lookAhead == NUM)
  {
    res = makeNumLeaf(numTokenVal);

    match(NUM);
  }
  else if (lookAhead == '(')
  {
    match('(');
    res = createNumExpSyntaxTree();
    match(')');
  }
  else if (lookAhead == VAR && isFunctionName(symTable[(int)numTokenVal].lexPtr))
  {
    res = numericFunction();
  }
  else if (lookAhead == VAR)
  {
    char *varName = symTable[(int)numTokenVal].lexPtr;
  
    if(isStringVar(varName))
      error("Found string variable in numeric expression");
    if(!isNumericVar(varName))
      error("Invalid variable name in expression");

    match(VAR);

    res = makeVarLeaf(varName);

    if(lookAhead == '(')
    {
      Node *newRes;
      Node *arrayVar1;
      Node *arrayVar2;

      assertArrayVar(varName);

      seenArray = 1;

      match('(');

      arrayVar1 = createNumExpSyntaxTree();

      if(lookAhead == ',')
      {
        match(',');
        assertConsistentArraySize(varName[0],2);

        arrayVar2 = createNumExpSyntaxTree();

        match(')');
      }
      else
      {
        match(')');
        assertConsistentArraySize(varName[0],1);

        arrayVar2 = makeNumLeaf(optionBase);
      }

      newRes = malloc(sizeof(Node));

      newRes->operation = ARRAY;
      newRes->numChildren = 3;
      newRes->children = (Node**)malloc(3*sizeof(Node*));
      newRes->children[0] = res;
      newRes->children[1] = arrayVar1;
      newRes->children[2] = arrayVar2;

      res = newRes;
    }
  }
  else
  {
    fprintf(stderr,"Found %c\n",lookAhead);
    error("Expected number, numeric variable or (");
  }
    
  return res;
}

Node *numericFunction()
{
  int fi,currArg;
  char *funcName;
  char msg[64];
  Node *res;

  #ifdef DEBUG_PARSE
    fprintf(stderr,"numericFunction\n");
  #endif

  funcName = symTable[(int)numTokenVal].lexPtr;

  fi = assertFunctionDefined(funcName);

  match(VAR);

  res = (Node*)malloc(sizeof(Node));

  res->operation = FUNC;
  res->numChildren = 1+functions[fi].numArgs;
  res->children = (Node**)malloc((res->numChildren)*sizeof(Node*));

  res->children[0] = makeVarLeaf(functions[fi].asmName);

  if(functions[fi].numArgs == 0 && lookAhead == '(')
  {
    sprintf(msg,"Function %s has no arguments\n",funcName);
    error(msg);
  }

  if(functions[fi].numArgs > 0 && lookAhead != '(')
  {
    sprintf(msg,"Function %s is missing argument(s)\n",funcName);
    error(msg);
  }

  if(lookAhead == '(')
  {
    match('(');
    currArg = 0;

    if(lookAhead == ')')
    {
      sprintf(msg,"Missing argument(s) to function %s\n",funcName);
      error(msg);
    }

    res->children[currArg+1] = createNumExpSyntaxTree();
    currArg+=1;

    while(lookAhead == ',')
    {
      currArg+=1;
      if(currArg >= functions[fi].numArgs)
      {
        sprintf(msg,"Too many arguments supplied to %s\n",funcName);
        error(msg);
      }

      match(',');

      res->children[currArg] = createNumExpSyntaxTree();
    }

    if(currArg < functions[fi].numArgs)
    {
      sprintf(msg,"Missing argument(s) for function %s\n",funcName);
      error(msg);
    }

    match(')');
  }

  return res;
}

Node *makeNode(int operation,Node *leftChild,Node *rightChild)
{
  Node *res;

  res = (Node*)malloc(sizeof(Node));

  res->operation = operation;

  res->numChildren = 2;
  res->children = (Node**)malloc(2*sizeof(Node*));

  res->children[0] = leftChild;
  res->children[1] = rightChild;

  return res;
}

Node *makeVarLeaf(char *varName)
{
  Node *res;

  res = (Node*)malloc(sizeof(Node));

  res->operation = VAR;
  res->numChildren = 0;
  res->varName = varName;

  return res;
}

Node *makeNumLeaf(double num)
{
  Node *res;

  res = (Node*)malloc(sizeof(Node));

  res->operation = NUM;
  res->numChildren = 0;
  res->num = num;

  return res;
}


void printStatement()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"printStatement\n");
  #endif
  match(PRINT);
  emitPrint();

  if(lookAhead == STR || lookAhead == TAB || lookAhead == VAR || lookAhead == NUM || lookAhead == '(' || lookAhead == ',' || lookAhead == ';' || lookAhead == '-' || lookAhead == '+')
    printList();
}

void printList()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"printList\n");
  #endif
  if(lookAhead == STR || lookAhead == TAB || lookAhead == VAR || lookAhead == NUM || lookAhead == '(' || lookAhead == '-' || lookAhead == '+')
    printItem();
  while(lookAhead != '\n' && lookAhead != DONE)
  {
    printSeparator();
    if(lookAhead == STR || lookAhead == VAR || lookAhead == TAB || lookAhead == NUM || lookAhead == '(' || lookAhead == '-' || lookAhead == '+')
      printItem();
  }
}

void printItem()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"printItem\n");
  #endif
  if(lookAhead == STR)
  {
    char *t = strTokenVal;
    match(STR);
    emitPrintStringConst(t);
  }
  else if (lookAhead == TAB)
  {
    match(TAB);
    match('(');
    numericExpression();
    match(')');
 
    emitTab();
  }
  else if (lookAhead == VAR && isStringVar(symTable[(int)numTokenVal].lexPtr))
  {
    emitPrintStringVar((int)numTokenVal);
    match(VAR);
  }
  else /* May need a test here */
  {
    numericExpression();
    emitPrintExpression();
  }
}


void printSeparator()
{
  #ifdef DEBUG_PARSE
    fprintf(stderr,"printSeparator\n");
  #endif
  if(lookAhead == ',')
  {
    match(lookAhead);
    emitPrintingComma();
  }
  else if(lookAhead == ';')
  {
    match(lookAhead);
    emitPrintingSemicolon();
  }
  else
    error("Expected print separator");
}

void endStatement()
{
  match(END);
  sawEnd = 1;
}

void stopStatement()
{
  emitStop();
  match(STOP);
}

void match(int t)
{
  if (lookAhead == t)
  {
    #ifdef DEBUG
      fprintf(stderr,"lookAhead = %d\n",lookAhead);
    #endif
    lookAhead = lexan();
  }
  else if (lookAhead == DONE)
  {
    fprintf(stderr,"Expected %d, found DONE\n",t);
    error("Missing END statement");
  }
  else
  {
    fprintf(stderr,"Line %d: Expected '",lineNumber);
    printType(t);
    fprintf(stderr,"', found '");
    printType(lookAhead);
    fprintf(stderr,"'\n");
    exit(1);
  }
}

void printType(int type)
{
  char msg[64];
  if(type >= 32 && type <= 126)
    sprintf(msg,"%c",type);
  else if (type == 10)
    sprintf(msg,"NEWLINE");
  else if (type > 255)
  {
    switch(type)
    {
      case NUM:
        sprintf(msg,"NUM");
        break;
      case STR:
        sprintf(msg,"STR");
        break;
      case PRINT:
        sprintf(msg,"PRINT");
        break;
      case END:
        sprintf(msg,"END");
        break;
      case DONE:
        sprintf(msg,"DONE");
        break;
      case VAR:
        sprintf(msg,"VAR");
        break;
      case SETUP:
        sprintf(msg,"SETUP");
        break;
      case TAB:
        sprintf(msg,"TAB");
        break;
      case STOP:
        sprintf(msg,"STOP");
        break;
      case LET:
        sprintf(msg,"LET");
        break;
      case REM:
        sprintf(msg,"REM");
        break;
      case GOTO:
        sprintf(msg,"GOTO");
        break;
      case GO:
        sprintf(msg,"GO");
        break;
      case TO:
        sprintf(msg,"TO");
        break;
      case GOSUB:
        sprintf(msg,"GOSUB");
        break;
      case SUB:
        sprintf(msg,"SUB");
        break;
      case RETURN:
        sprintf(msg,"RETURN");
        break;
      case IF:
        sprintf(msg,"IF");
        break;
      case THEN:
        sprintf(msg,"THEN");
        break;
      case EQUALS:
        sprintf(msg,"=");
        break;
      case NOTEQUALS:
        sprintf(msg,"<>");
        break;
      case LESSTHAN:
        sprintf(msg,"<");
        break;
      case GREATERTHAN:
        sprintf(msg,">");
        break;
      case NOTLESS:
        sprintf(msg,">=");
        break;
      case NOTGREATER:
        sprintf(msg,"<=");
        break;
      case FOR:
        sprintf(msg,"FOR");
        break;
      case NEXT:
        sprintf(msg,"NEXT");
        break;
      case STEP:
        sprintf(msg,"STEP");
        break;
      case DIM:
        sprintf(msg,"DIM");
        break;
      case OPTION:
        sprintf(msg,"OPTION");
        break;
      case BASE:
        sprintf(msg,"BASE");
        break;
      case ON:
        sprintf(msg,"ON");
        break;
      case READ:
        sprintf(msg,"READ");
        break;
      case RESTORE:
        sprintf(msg,"RESTORE");
        break;
      case DATA:
        sprintf(msg,"DATA");
        break;
      case INPUT:
        sprintf(msg,"INPUT");
        break;
      case ARRAY:
        sprintf(msg,"ARRAY");
        break;
      case FUNC:
        sprintf(msg,"FUNC");
        break;
      case RANDOMIZE:
        sprintf(msg,"RANDOMIZE");
        break;
      case DEF:
        sprintf(msg,"DEF");
        break;
      case FNVAR:
        sprintf(msg,"FNVAR");
        break;
      default:
        sprintf(msg,"undef type %d",type);
    }
  }
  else
  {
    sprintf(msg,"undef small type %d",type);
  }

  fprintf(stderr,"%s",msg);
}


void addFunction(char *basicName,char *asmName,int numArgs,int declaredLine)
{
  functions[lastFuncEntry].basicName = malloc(strlen(basicName)+1);
  functions[lastFuncEntry].asmName = malloc(strlen(asmName)+1);

  strcpy(functions[lastFuncEntry].basicName,basicName);
  strcpy(functions[lastFuncEntry].asmName,asmName);
  functions[lastFuncEntry].numArgs = numArgs;
  functions[lastFuncEntry].declaredLine = declaredLine;

  lastFuncEntry++;

  if(lastFuncEntry >= 50)
    error("Exceede maximum number of function definitions");
}


int isInteger(double num)
{
  return num == (int)num;
}

int isStringVar(char *str)
{
  return (str[0] >= 'A' && str[0] <= 'Z' && str[1] == '$' && str[2] == 0);
}

int isNumericVar(char *str)
{
  if(str[0] >= 'A' && str[0] <= 'Z' && str[1] == 0)
    return 1;

  if(str[0] >= 'A' && str[0] <= 'Z' && str[1] >='0' && str[1] <= '9' && str[2] == 0)
    return 1;

  return 0;
}

int isFunctionName(char *str)
{
  return str[0] >= 'A' && str[0] <= 'Z' &&
         str[1] >= 'A' && str[1] <= 'Z' &&
         str[2] >= 'A' && str[2] <= 'Z' &&
         str[3] == 0;
}


void assertUserDefFunctionName(char *name)
{
  if(!(name[0] == 'F' && name[1] == 'N' && 
       name[2] >= 'A' && name[2] <= 'Z' &&
       name[3] == 0))
    error("Invalid function name");
}

void assertFunctionNotDeclared(char *name)
{
  int i;

  for(i=0;i<lastFuncEntry;++i)
    if(functions[i].basicName[0] == 'F' && 
       functions[i].basicName[1] == 'N' &&
       functions[i].basicName[2] == name[2])
      error("Function can only be declared once");
}

int assertFunctionDefined(char *name)
{
  int i;
  char msg[64];

  for(i=0;i<lastFuncEntry;++i)
    if(functions[i].basicName[0] == name[0] && functions[i].basicName[1] == name[1] &&
       functions[i].basicName[2] == name[2])
      return i;

  sprintf(msg,"Undefined function %s\n",name);
  error(msg);
  return -1;
}

void assertInteger(double d)
{
  if(d != (int)d)
    error("Expected integer, found double");
}

void assertStringVar(char *str)
{
  char msg[50];

  if(!isStringVar(str))
  {
    sprintf(msg,"Invalid string variable name %s",str);
    error(msg);
  }
}

void assertArrayVar(char *str)
{
  if(!isNumericVar(str) || str[1] != 0)
    error("Invalid array variable name");
}

void assertNumericVar(char *str)
{
  if(!isNumericVar(str))
    error("Invalid numeric variable name");
}

void assertStringSize(char *str)
{
  if (strlen(str) >= 32)
    error("string too long");
}

void assertForLoopUnique(char *var)
{
  int i;
  for(i=0;i<flvIndex;++i)
  {
    if(forLoopVars[i][0] == var[0] && forLoopVars[i][1] == var[1])
    {
      error("FOR loop using previous control variable");
    }
  }
}

void assertFutureLineNums(int num)
{
  int i;
  for(i=0;i<fnBack;++i)
  {
    if(futureNumbers[i][2] == 0 && num > futureNumbers[i][0])
    {
      fprintf(stderr,"Line %d: could not find line number %d\n",futureNumbers[i][1],futureNumbers[i][0]);
      exit(1);
    }
    else if (num == futureNumbers[i][0])
    {
      futureNumbers[i][2] = 1;
    }
  }
}

void assertLineNumExists(int lineNum)
{
  int i;

  if(lineNumbers[lnIndex-1] < lineNum)
  {
    futureNumbers[fnBack][0] = lineNum;
    futureNumbers[fnBack][1] = lineNumber;
    futureNumbers[fnBack][2] = 0;
    fnBack+=1;
  }
  else
  {
    i=0;
    while(lineNumbers[i] < lineNum)
      i++;

    if(lineNumbers[i] > lineNum)
    {
      char msg[40];
      sprintf(msg,"line number %d does not exist",lineNum);
      error(msg);
    }
  }
}

void assertConsistentArraySize(char var,int size)
{
  if(arraySize[var-'A'] == 0)
    arraySize[var-'A'] = size;
 
  if(arraySize[var-'A'] != size)
    error("Array reference incompatible with previous dim");
}

void assertNoJumpsIntoForLoops()
{
  int i,j;

  for(i=0;i<gdIndex;++i)
  {
    for(j=0;j<flbIndex;++j)
    {
      if(forLoopBlocks[j][0] < gotoDests[i][0] && 
         gotoDests[i][0] <= forLoopBlocks[j][1] &&
         (gotoDests[i][1] < forLoopBlocks[j][0] ||
          gotoDests[i][1] > forLoopBlocks[j][1]))
      {
        lineNumber = gotoDests[i][1];
        error("Cannot jump into a FOR block");
      }
    }
  }
}

