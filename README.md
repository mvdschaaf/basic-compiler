# BASIC Compiler

This is my attempt at making an ECMA-55 BASIC compiler, using the NBS tests to confirm compliance.

This compiler will probably not comply 100%. My goal is to make a single pass compiler to LLVM IR. Due to the limitations of both of those goals, some of the requirements of ECMA-55 may not be met.

## Default values

Because GOTO and other control statements can skip over LET statements, all variables are initialized to a default value. For strings, it is a blank string. For numeric variables, it is 0.

## NBS Tests
Tests 1-208 pass, except for those below

9,10,12,14 don't pass due to imperfections in the number formatting of PRINT

29,30,35,101,112 don't pass due to not detecting overflow

62 doesn't pass because DIM statements are run and the sizes saved at that point. In order for this to pass, all DIM statements should be run at the start of the program.

203 doesn't pass because the print statement doesn't wrap properly when going past the margin

131,133,135 appear not to pass in the automated tests, but they have RANDOMIZE so they never will


## Automated testing

I've written a very simple script to automatically run all of the NBS tests.
Due to its simplicity, if a test that has input fails, the script will hang.

## Todo

Remake TAB a builtin function

Write unit tests for both lexer and inputLexer

Write a custom PRINT function instead of using printf

Write the math library instead of using math.h (Get a copy of Computer Approximations)
