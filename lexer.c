/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"
#include "conio.h"
#include "error.h"
#include "symbol.h"

char lexBuf[BSIZE];

int t = -1;

int isQuotedStringChar(char);
int isUnquotedStringChar(char);
int isPlainStringChar(char);
int digit(char);
int letter(char);

int lexan()
{
  int b,state;

  state = 0;

  for(b=0;b<BSIZE;++b)
    lexBuf[b] = 0;

  b = 0;

  if(t == -1)
    t = getch();


  while(1)
  {
#ifdef DEBUG
    fprintf(stderr,"State: %d, t: %d\n",state,t);
#endif
    if(state == 0)
    {
      if(t >= 'A' && t <= 'Z')
      {
        lexBuf[b++] = t;
        state = 1;
        t = getch();
      }
      else if (t >= '0' && t <= '9')
      {
        lexBuf[b++] = t;
        state = 2;
        t = getch();
      }
      else if (t == '.')
      {
        lexBuf[b++] = t;
        state = 4;
        t = getch();
      }
      else if (t == '\r')
      {
        t = getch();
        if(t == '\n')
          t = getch();
        return '\n';
      }
      else if (t == '\n' || t == ',' || t == ';' || t == '(' || t == ')' || t == '-' || t == '+' || t == '*' || t == '/' || t == '^')
      {
        int retVal = t;
        t = getch();
        return retVal;
      }
      else if (t == '"')
      {
        state = 3;
        t = getch();
      }
      else if (t == ' ' || t == '\t')
      {
        t = getch();
      }
      else if (t == '=' || t == '<' || t == '>')
      {
        char first;
        first = t;

        t = getch();
        if((first == '=' && t == '>') ||
           (first == '=' && t == '=') ||
           (first == '=' && t == '<') ||
           (first == '<' && t == '<')
           )
        {
          char msg[30];
          sprintf(msg,"Unexpected token '%c%c'",first,t);
          error(msg);
        }
        else if (first == '=')
        {
          return EQUALS;
        } 
        else if (first == '<' && t == '=')
        {
          t = getch();
          return NOTGREATER;
        }
        else if (first == '>' && t == '=')
        {
          t = getch();
          return NOTLESS;
        }
        else if (first == '<' && t == '>')
        {
          t = getch();
          return NOTEQUALS;
        }
        else if (first == '<')
        {
          return LESSTHAN;
        }
        else if (first == '>')
        {
          return GREATERTHAN;
        }
      }
      else if (t == EOF)
      {
        return DONE;
      }
      else
      {
        char msg[50];

        sprintf(msg,"Unexpected character '%c'",t);
        error(msg);
      }
    }
    else if (state == 1)
    {
      if ((t >= 'A' && t <= 'Z') || t == '$' || (t >= '0' && t <= '9'))
      {
        lexBuf[b++] = t;
        t = getch();
      }
      else if (t == ' ' || t == '\t' || t == '\r' || t == '\n' || t == ',' || t == ';' || t == '(' || t == ')' || t == '=' || t == '<' || t == '>' || t == '+' || t == '-' || t == '*' || t == '/' || t == '^' || t == EOF)
      {
        int p = 0;

        p = lookup(lexBuf);

        if (p == 0)
          p = insert(lexBuf, VAR);

        numTokenVal = p;

        return symTable[p].token;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
    else if (state == 2) /* Reading a number */
    {
      if (t >= '0' && t <= '9')
      {
        lexBuf[b++] = t;
        t = getch();
      }
      else if (t == '.')
      {
        lexBuf[b++] = t;
        t = getch();
        state = 4;
      }
      else if (t == 'E')
      {
        lexBuf[b++] = t;
        t = getch();
        state = 5;
      }
      else if (t == ' ' || t == ')' || t == ',' || t == ';' || t == '=' || t == '+' || t == '-' || t == '*' || t == '/' || t == '^' || t == '<' || t == '>' || t == '\t' || t == '\r' || t == '\n')
      {
        sscanf(lexBuf,"%lf",&numTokenVal);

        return NUM;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
    else if (state == 3) /* Reading a string */
    {
      if(t == '\n' || t == '\r')
      {
        fprintf(stderr,"%d: Error: Unexpected new line in string\n",lineNumber);
        exit(1);
      }
      else if (isQuotedStringChar(t))
      {
        lexBuf[b++] = t;
        t = getch();
      }
      else if (t == '"')
      {
        char *buffer = malloc(b+1);
        strcpy(buffer,lexBuf);
        strTokenVal = buffer;

        t = getch();

        return STR;
      } 
      else
      {
        fprintf(stderr,"%d: Error unexpected character '%c' in string\n",lineNumber,t);
        exit(1);
      }
    }
    else if (state == 4) /* Reading number, saw a decimal point but not the E */
    {
      if (t >= '0' && t <= '9')
      {
        lexBuf[b++] = t;
        t = getch();
      }
      else if (t == ' ' || t == ')' || t == ',' || t == ';' || t == '=' || t == '<' || t == '>' || t == '+' || t == '-' || t == '*' || t == '/' || t == '^' || t == '\t' || t == '\r' || t == '\n')
      {
        sscanf(lexBuf,"%lf",&numTokenVal);
        return NUM;
      }
      else if (t == 'E')
      {
        lexBuf[b++] = t;
        t = getch();
        state = 5;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
    else if (state == 5) /* Reading number, seen an E */
    {
      if (t == '+' || t == '-' || (t >= '0' && t <= '9'))
      {
        lexBuf[b++] = t;
        t = getch();
        state = 6;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
    else if (state == 6) /* Reading number, seen an E and the sign for the exponent */
    {
      if (t >= '0' && t <= '9')
      {
        lexBuf[b++] = t;
        t = getch();
      }
      else if (t == ' ' || t == '\r' || t == '\n' || t == '+' || t == '-' || t == '*' || t == '/' || t == '^' || t == ',' || t == ';' || t == ')')
      {
        float fnum;
        sscanf(lexBuf,"%E",&fnum);
        numTokenVal = fnum;

        return NUM;
      }
      else
      {
        fprintf(stderr,"Unexpected character '%c' at line %d\n",t,lineNumber);
        exit(1);
      }
    }
  }
}

int isQuotedStringChar(char c)
{
  return (c >= '!' && c <= ',' && c != '"' && c != '+') ||
         (c == '/') || (c >= ':' && c <= '?') || c == '^' || c == '_' ||
         isUnquotedStringChar(c);
}

int isUnquotedStringChar(char c)
{
  return c == ' ' || isPlainStringChar(c);
}

int isPlainStringChar(char c)
{
  return c == '+' || c == '-' || c == '.' || digit(c) || letter(c);
}

int digit(char c)
{
  return c >= '0' && c <= '9';
}

int letter(char c)
{
  return c >= 'A' && c <= 'Z';
}

void lexanRem()
{
  while(t != '\r' && t != '\n')
    t = getch();
}

char *lexanData()
{
  int b;

  for(b=0;b<BSIZE;++b)
    lexBuf[b] = 0;

  b = 0;

  while (t != '\r' && t != '\n')
  {
    lexBuf[b] = t;
    b++;
    t = getch();
  }

  return lexBuf;
}
