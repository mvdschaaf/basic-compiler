#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
passed=0
total=208

failString="Tests "

for i in `seq 1 $total`
do

  filename="NBS Tests/expected/${i}.out"

  if [ ! -f "$filename" ]
  then
    filename="NBS Tests/expected/${i}.err"
    ./bcc.sh "NBS Tests/${i}.bas" 2> temp.err
    diff --brief "temp.err" "$filename" > /dev/null 
  else
    ./bcc.sh "NBS Tests/${i}.bas" &> /dev/null
    infile="NBS Tests/expected/${i}.in"
    if [ ! -f "$infile" ]
    then
      diff --brief <(./a.out) "$filename" > /dev/null
    else
      diff --brief <(./a.out < "$infile") "$filename" > /dev/null
    fi
  fi
  comp_value=$?

  if [ $comp_value -eq 1 ]
  then
    failString="$failString ${i},"
    printf "${RED}.${NC}" 219
  else
    let "passed+=1"
    printf "${GREEN}.${NC}"
  fi

  if [ $[$i % 50] -eq 0 ]
  then
    echo
  fi

done

rm temp.err

echo

echo $passed "tests passed out of" $total
echo ${failString:0:-1} failed
