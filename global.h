/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef ___GLOBAL_H___
#define ___GLOBAL_H___
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BSIZE 128
#define NONE -1
#define EOS '\0'

#define SYMMAX 200
#define CALLMAX 256

#define NUM          256
#define STR          257
#define PRINT        258
#define END          259
#define DONE         260
#define VAR          261
#define SETUP        262
#define TAB          263
#define STOP         264
#define LET          265
#define REM          266
#define GOTO         267
#define GO           268
#define TO           269
#define GOSUB        270
#define SUB          271
#define RETURN       272
#define IF           273
#define THEN         274
#define EQUALS       275
#define NOTEQUALS    276
#define LESSTHAN     277
#define GREATERTHAN  278
#define NOTLESS      279
#define NOTGREATER   280
#define FOR          281
#define NEXT         282
#define STEP         283
#define DIM          284
#define OPTION       285
#define BASE         286
#define ON           287
#define READ         288
#define RESTORE      289
#define DATA         290
#define INPUT        291
#define ARRAY        292
#define FUNC         293
#define RANDOMIZE    294
#define DEF          295
#define FNVAR        296

static const double POS_INF =  10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0;
static const double NEG_INF = -10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.0;

char *strTokenVal;
double numTokenVal;

int lineNumber;
int tempVarNum;

struct entry {
  char *lexPtr;
  int token;
};

struct entry symTable[SYMMAX];

#endif
