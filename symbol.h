/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __SYMBOL__H__
#define __SYMBOL__H__

int lookup(char []);
int insert(char [],int);

#endif
