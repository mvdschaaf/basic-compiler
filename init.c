/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"
#include "symbol.h"

struct entry keywords[] = {
    {"PRINT", PRINT},
    {"END",   END},
    {"TAB",   TAB},
    {"STOP",  STOP},
    {"LET",   LET},
    {"REM",   REM},
    {"GOTO",  GOTO},
    {"GO",    GO},
    {"TO",    TO},
    {"GOSUB", GOSUB},
    {"SUB",   SUB},
    {"RETURN",RETURN},
    {"IF",    IF},
    {"THEN",  THEN},
    {"FOR",   FOR},
    {"NEXT",  NEXT},
    {"STEP",  STEP},
    {"DIM",   DIM},
    {"OPTION",OPTION},
    {"BASE",  BASE},
    {"ON",    ON},
    {"READ",  READ},
    {"RESTORE",RESTORE},
    {"DATA",  DATA},
    {"INPUT", INPUT},
    {"FUNC",  FUNC},
    {"RANDOMIZE",RANDOMIZE},
    {"DEF",   DEF},
    {0,       0}
};

void init()
{
  struct entry *p;
  for (p = keywords; p->token; p++)
    insert(p->lexPtr, p->token);
}
