#include <termios.h>
#include <unistd.h>

int getch()
{
  struct termios oldterm, newterm;
  int ch;
  tcgetattr( STDIN_FILENO, &oldterm );
  newterm = oldterm;
  newterm.c_lflag &= ~( ICANON | ECHO );
  tcsetattr( STDIN_FILENO, TCSANOW, &newterm );
  ch = getchar();
  tcsetattr( STDIN_FILENO, TCSANOW, &oldterm );
  return ch;
}
