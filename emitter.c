/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"
#include "error.h"
#include "builtin/inputLexer.h"
#include "parser.h"

#define STRMAX 9999

char constStrs[STRMAX];
int lastStrChar = 0;
int numStrings = -1;

int gosubReturnLocs[500];
int gsrlTop = 0;

int printing = 0;
int lastSeparatorSemicolon = 0;

datum dataStorage[1024];
int lastData = 0;

void addString(char []);
void emitStringConstants();
void emitStringVariables();
void emitNumericConstants();

void emitReturnUnderflowTest();
void emitSubtractOneFromCallStackTop();
void emitReturnFunc(short[],int);
void emitReturnLocs(short[],int);
int findNextLine(int,short[],int);
void emitOnGotoError();

void emitForLoopVariables(int);
void emitNumericVariables();
void emitNumericRep(double);
void emitNumericVar(char*);
void emitAddition(int,int);
void emitNumericLet(char*);

void emitStoredData();
void emitStoredDataString(char*);

void printDouble(double);

void addReturnLoc(int);

void emitSetup()
{
  tempVarNum = 1;

  printf("%%struct.d = type { [32 x i8], double, i32 }\n");
  printf("declare void @print(i8*) #1\n");
  printf("declare void @printTab(double,i32) #1\n");
  printf("declare void @printNewLine(...) #1\n");
  printf("declare void @printNumber(double) #1\n");
  printf("declare void @printComma(...) #1\n");
  printf("declare void @exit(i32) #2\n");
  printf("declare void @randomize(...) #1\n");

  printf("declare i8* @strcpy(i8*, i8*) #1\n");
  printf("declare i32 @stringComp(i8*, i8*) #2\n");

  printf("declare void @assignValue(i8 signext, double, double, double, i32) #1\n");
  printf("declare double @retrieveValue(i8 signext, double, double, i32) #1\n");
  printf("declare void @setArraySize(i8 signext, i32, i32, i32) #1\n");
  printf("declare void @setOptionBase(i32) #1\n");

  printf("declare void @restore(...) #1\n");
  printf("declare i8* @readString(i32) #1\n");
  printf("declare double @readNum(i32) #1\n");

  printf("declare %%struct.d* @basicInput(i32, i32, i32) #1\n");

  printf("declare double @basicDiv(double,double,i32) #1\n");
  printf("declare double @basicPow(double,double,i32) #1\n");

  printf("declare void @exception(i8*,i32) #1\n");
  printf("define i32 @main() #0\n{\n");
}

void emitRandomize()
{
  printf("call void (...)* @randomize()\n");
}

void emitDefinedFunctions(FuncDec *funcs,int numFuncs)
{
  int i,j;

  for(i=0;i<numFuncs;++i)
  {
    if(funcs[i].basicName[0] != 'F' || funcs[i].basicName[1] != 'N')
    {
      printf("declare double @%s(",funcs[i].asmName);
      for(j=0;j<funcs[i].numArgs;++j)
        printf("double,");
      printf("i32) #1\n");
    }
  }
}

void emitCallFunction(char *name,int args[],int numArgs)
{
  int i;

  printf("%%%d = call double @%s(",tempVarNum,name);
  for(i=0;i<numArgs;++i)
    printf("double %%%d,",args[i]);
  printf("i32 %d)\n",lineNumber);

  tempVarNum+=1;
}
void emitUserDefFunStart(char name,char *varName,int numArgs)
{
  tempVarNum = 1;
  printf("define double @FN%c(",name);
  if(numArgs > 0)
    printf("double %%%s,",varName);
  printf("i32 %%line) #0 {");
  if(numArgs > 0)
  {
    tempVarNum = 2;
    printf("%%1 = alloca double, align 8\n");
    printf("store double %%%s, double* %%1, align 8\n",varName);
  }
}
    
/* 
define double @fnp(double %x) #0 {
  %1 = alloca double, align 8
  store double %x, double* %1, align 8
*/
void emitUserDefFunEnd(char *varName)
{
  printf("ret double %%%d\n}\n",tempVarNum-1);
}

void emitPrint()
{
  printing = 1;
  lastSeparatorSemicolon = 0;
}

void emitNewline()
{
  if(printing == 1 && lastSeparatorSemicolon == 0)
  {
    printf("call void (...)* @printNewLine()\n");
  }
  printing = 0;
}

void emitInput(int types,int numInputs)
{
  printf("%%%d = call %%struct.d* @basicInput(i32 %d, i32 %d, i32 %d)\n",tempVarNum,types,numInputs,lineNumber);
  tempVarNum++;
}

void emitInputLetNum(char *varName,int dataVarNum,int dataLoc)
{
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 %d\n",tempVarNum,dataVarNum,dataLoc);
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 0, i32 1\n",tempVarNum+1,tempVarNum);
  printf("%%%d = load double* %%%d, align 4",tempVarNum+2,tempVarNum+1);
  tempVarNum+=3;
  emitNumericLet(varName);
}

void emitInputLetStr(char *varName,int dataVarNum,int dataLoc)
{
  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum,varName[0]);
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 %d\n",tempVarNum+1,dataVarNum,dataLoc);
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 0, i32 0\n",tempVarNum+2,tempVarNum+1);
  printf("%%%d = getelementptr inbounds [32 x i8]* %%%d, i32 0, i32 0\n",tempVarNum+3,tempVarNum+2);
  printf("%%%d = call i8* @strcpy(i8* %%%d, i8* %%%d)\n",tempVarNum+4,tempVarNum,tempVarNum+3);

  tempVarNum+=5;
}

void emitInputLetArray(char *varName,int rowVar,int colVar,int dataVarNum,int dataLoc)
{
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 %d\n",tempVarNum,dataVarNum,dataLoc);
  printf("%%%d = getelementptr inbounds %%struct.d* %%%d, i32 0, i32 1\n",tempVarNum+1,tempVarNum);
  printf("%%%d = load double* %%%d, align 8\n",tempVarNum+2,tempVarNum+1);
  printf("call void @assignValue(i8 signext %d,double %%%d,double %%%d,double %%%d,i32 %d)\n",(int)varName[0],rowVar,colVar,tempVarNum+2,lineNumber);
  tempVarNum+=3;
}

void emitNumericRead(char *varName)
{
  printf("%%%d = call double @readNum(i32 %d)\n",tempVarNum,lineNumber);
  tempVarNum+=1;
  emitNumericLet(varName);
}

void emitStringRead(char *varName)
{
  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum,varName[0]);
  printf("%%%d = call i8* @readString(i32 %d)\n",tempVarNum+1,lineNumber);
  printf("%%%d = call i8* @strcpy(i8* %%%d, i8* %%%d)\n",tempVarNum+2,tempVarNum,tempVarNum+1);

  tempVarNum+=3;
}

void emitArrayRead(char *varName,int rowVar,int colVar)
{
  printf("%%%d = call double @readNum(i32 %d)\n",tempVarNum,lineNumber);
  printf("call void @assignValue(i8 signext %d,double %%%d,double %%%d,double %%%d,i32 %d)\n",(int)varName[0],rowVar,colVar,tempVarNum,lineNumber);
  tempVarNum+=1;
}

void emitRestore()
{
  printf("call void (...)* @restore()");
}

void emitData(datum *data, int numData)
{
  int i;

  for(i=0;i<numData;++i)
  {
    strcpy(dataStorage[lastData].str,data[i].str);
    dataStorage[lastData].num = data[i].num;
    dataStorage[lastData].isValidNum = data[i].isValidNum;

    lastData++;
  }
}

void emitOnGoto(int conditionVar,int dests[],int numDests)
{
  int i;

  printf("%%%d = fadd double %%%d, 5.000000e-01\n",tempVarNum,conditionVar);
  printf("%%%d = fptosi double %%%d to i32\n",tempVarNum+1,tempVarNum);
  printf("store i32 %d, i32* @onGotoCallLineNo, align 4\n",lineNumber);
  printf("switch i32 %%%d, label %%onGotoDefault [ ",tempVarNum+1);

  for(i=0;i<numDests;++i)
    printf("i32 %d, label %%L%d\n",i+1,dests[i]);
  printf("]\n");

  tempVarNum+=3;
} 

void emitForLoopStart(int loopNum,char *controlVar,int initValVarNum,int limitVarNum, int stepVarNum)
{
  printf("store double %%%d, double* @forLoopLimit%d, align 8\n",limitVarNum,loopNum);
  printf("store double %%%d, double* @forLoopInc%d, align 8\n",stepVarNum,loopNum);
  printf("store double %%%d, double* @numVar%s, align 8\n",initValVarNum,controlVar);
  printf("br label %%forLoopStart%d\n",loopNum);
  printf("forLoopStart%d:\n",loopNum);
  
  printf("%%%d = load double* @numVar%s, align 8\n",tempVarNum,controlVar);
  printf("%%%d = load double* @forLoopLimit%d, align 8\n",tempVarNum+1,loopNum);
  printf("%%%d = fsub double %%%d, %%%d\n",tempVarNum+2,tempVarNum,tempVarNum+1);
  printf("%%%d = load double* @forLoopInc%d, align 8\n",tempVarNum+3,loopNum);
  printf("%%%d = call double @basicSgn(double %%%d, i32 %d)\n",tempVarNum+4,tempVarNum+3,lineNumber);
  printf("%%%d = fmul double %%%d, %%%d\n",tempVarNum+5,tempVarNum+2,tempVarNum+4);
  printf("%%%d = load double *@myZero, align 8\n",tempVarNum+6);
  printf("%%%d = fadd double %%%d, 0.0000000e+00\n",tempVarNum+7,tempVarNum+6);
  printf("%%%d = fcmp ogt double %%%d, %%%d\n",tempVarNum+8,tempVarNum+5,tempVarNum+7);
  printf("br i1 %%%d, label %%%d, label %%%d\n",tempVarNum+8,tempVarNum+9,tempVarNum+10);
  printf("; <label>:%%%d\n",tempVarNum+9);
  printf("br label %%forLoopEnd%d\n",loopNum);
  printf("; <label>:%%%d\n",tempVarNum+10);

  tempVarNum+=11;
}

void emitDefaultStep()
{
  printf("%%%d = load double *@myZero, align 8\n",tempVarNum);
  printf("%%%d = fadd double %%%d, 1.0000000e+00\n",tempVarNum+1,tempVarNum);

  tempVarNum+=2;
}

void emitNext(int loopNum,char *controlVar,int stepTempVar)
{
  printf("%%%d = load double* @numVar%s, align 8\n",tempVarNum,controlVar);
  printf("%%%d = load double* @forLoopInc%d, align 8\n",tempVarNum+1,loopNum);
  printf("%%%d = fadd double %%%d, %%%d\n",tempVarNum+2,tempVarNum,tempVarNum+1);
  printf("store double %%%d, double* @numVar%s, align 8\n",tempVarNum+2,controlVar);
  printf("br label %%forLoopStart%d\n",loopNum);
  printf("br label %%forLoopEnd%d\n",loopNum);
  printf("forLoopEnd%d:\n",loopNum);

  tempVarNum += 4;
}

void emitStringVarVarComparison(char *var1,char *var2,int op,int destLine)
{
  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum,var1[0]);
  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum+1,var2[0]);
  printf("%%%d = call i32 @stringComp (i8* %%%d, i8* %%%d)\n",tempVarNum+2,tempVarNum,tempVarNum+1);

  printf("%%%d = icmp ne i32 %%%d, 0\n",tempVarNum+3,tempVarNum+2);
  if(op == EQUALS)
    printf("br i1 %%%d, label %%%d, label %%%d\n\n",tempVarNum+3,tempVarNum+4,tempVarNum+5);
  else
    printf("br i1 %%%d, label %%%d, label %%%d\n\n",tempVarNum+3,tempVarNum+5,tempVarNum+4);

  printf("; <label>:%d\n",tempVarNum+4);
  printf("br label %%L%d\n\n",destLine);

  printf("; <label>:%d\n",tempVarNum+5);

  tempVarNum += 6;
}

void emitStringVarConstComparison(char *var,char *str,int op,int destLine)
{
  int strSize;

  addString(str);
  strSize = strlen(str);

  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum,var[0]);
  printf("%%%d = getelementptr inbounds [%d x i8]* @.stringConstant%d, i32 0, i32 0\n",tempVarNum+1,strSize+1,numStrings);
  printf("%%%d = call i32 @stringComp (i8* %%%d, i8* %%%d)\n",tempVarNum+2,tempVarNum,tempVarNum+1);

  printf("%%%d = icmp ne i32 %%%d, 0\n",tempVarNum+3,tempVarNum+2);
  if(op == EQUALS)
    printf("br i1 %%%d, label %%%d, label %%%d\n\n",tempVarNum+3,tempVarNum+4,tempVarNum+5);
  else
    printf("br i1 %%%d, label %%%d, label %%%d\n\n",tempVarNum+3,tempVarNum+5,tempVarNum+4);

  printf("; <label>:%d\n",tempVarNum+4);
  printf("br label %%L%d\n\n",destLine);

  printf("; <label>:%d\n",tempVarNum+5);

  tempVarNum += 6;
}

void emitNumericComparison(int tempVar1,int tempVar2,int op,int destLine)
{
  char *opStr;

  switch(op)
  {
    case EQUALS:
      opStr = "oeq";
      break;
    case NOTEQUALS:
      opStr = "one";
      break;
    case LESSTHAN:
      opStr = "olt";
      break;
    case GREATERTHAN:
      opStr = "ogt";
      break;
    case NOTLESS:
      opStr = "oge";
      break;
    case NOTGREATER:
      opStr = "ole";
      break;
    default:
      error("Unknown operator");
  }

  printf("%%%d = fcmp %s double %%%d, %%%d\n",tempVarNum,opStr,tempVar1,tempVar2);
  printf("br i1 %%%d, label %%%d, label %%%d\n",tempVarNum,tempVarNum+1,tempVarNum+2);

  printf("; <label>:%d\n",tempVarNum+1);
  printf("br label %%L%d\n",destLine);

  printf("; <label>:%d\n",tempVarNum+2);

  tempVarNum+=3;
}

void emitDone(short lineNumbers[],int lnSize,int numLoops)
{
  printf("ret i32 0\n");
  emitOnGotoError();
  if(gsrlTop > 0)
    emitReturnFunc(lineNumbers,lnSize);
  printf("}\n\n");
  emitStringConstants();
  emitStringVariables();
  emitNumericConstants();
  emitNumericVariables();
  emitForLoopVariables(numLoops);
  emitStoredData();
}

void emitOnGotoError()
{
  printf("br label %%onGotoDefault\n");
  printf("onGotoDefault:\n");
  printf("%%%d = load i32* @onGotoCallLineNo, align 4\n",tempVarNum+1);
  printf("call void @exception(i8* getelementptr inbounds ([26 x i8]* @.onGotoExc, i32 0, i32 0), i32 %%%d)\n",tempVarNum+1);
  printf("ret i32 0\n");
  tempVarNum+=2;
}

void emitGosub(int src,int dest)
{
  printf("%%%d = load i32* @csTop, align 4\n",tempVarNum);
  printf("%%%d = getelementptr inbounds [%d x i32]* @callStack, i32 0, i32 %%%d\n",tempVarNum+1,CALLMAX,tempVarNum);
  printf("store i32 %d, i32* %%%d, align 4\n",src,tempVarNum+1);
  printf("%%%d = load i32* @csTop, align 4\n",tempVarNum+2);
  printf("%%%d = add nsw i32 %%%d, 1\n",tempVarNum+3,tempVarNum+2);
  printf("store i32 %%%d, i32* @csTop, align 4\n",tempVarNum+3);
  printf("br label %%L%d\n",dest);

  tempVarNum += 5;

  addReturnLoc(src);
}

void emitReturn()
{
   printf("store i32 %d, i32* @retCallLineNo, align 4\n",lineNumber);
   printf("br label %%returnFunc\n");
   tempVarNum+=1;
}

void emitReturnFunc(short lineNumbers[], int lnSize)
{
   printf("returnFunc:\n");

   emitReturnUnderflowTest();
   emitSubtractOneFromCallStackTop();
   emitReturnLocs(lineNumbers,lnSize);
}

void emitSubtractOneFromCallStackTop()
{
  printf("%%%d = load i32* @csTop, align 4\n",tempVarNum);
  printf("%%%d = add nsw i32 %%%d, -1\n",tempVarNum+1,tempVarNum);
  printf("store i32 %%%d, i32* @csTop, align 4\n",tempVarNum+1);
  tempVarNum += 2;
}

void emitReturnUnderflowTest()
{
  printf("%%%d = load i32* @csTop, align 4\n",tempVarNum);
  printf("%%%d = icmp eq i32 %%%d, 0\n",tempVarNum+1,tempVarNum);
  printf("br i1 %%%d, label %%retUnderflow, label %%noRetUnderflow\n\n",tempVarNum+1);
  printf("retUnderflow:\n");
  printf("%%%d = load i32* @retCallLineNo, align 4\n",tempVarNum+2);
  printf("call void @exception(i8* getelementptr inbounds ([16 x i8]* @.retUnderflowExc, i32 0, i32 0), i32 %%%d)\n",tempVarNum+2);
  printf("br label %%noRetUnderflow\n");
  printf("noRetUnderflow:\n");

  tempVarNum += 3;
}

void emitReturnLocs(short lineNumbers[], int lnSize)
{
  int i,callLineNo,nextLineNo;
  for(i=0;i<gsrlTop;++i)
  {
    callLineNo = gosubReturnLocs[i];
    nextLineNo = findNextLine(callLineNo,lineNumbers,lnSize);
   
    printf("%%%d = load i32* @csTop, align 4\n",tempVarNum);
    printf("%%%d = getelementptr inbounds [%d x i32]* @callStack, i32 0, i32 %%%d\n",tempVarNum+1,CALLMAX,tempVarNum);
    printf("%%%d = load i32* %%%d, align 4\n",tempVarNum+2,tempVarNum+1);
    printf("%%%d = icmp eq i32 %%%d, %d\n",tempVarNum+3,tempVarNum+2,callLineNo);
    printf("br i1 %%%d, label %%%d, label %%%d\n",tempVarNum+3,tempVarNum+4,tempVarNum+5);
    printf("; <label>:%d\n",tempVarNum+4);
    printf("br label %%L%d\n",nextLineNo);
    printf("; <label>:%d\n",tempVarNum+5);

    tempVarNum+=6;
  }

  printf("call void @exception(i8* getelementptr inbounds ([16 x i8]* @.retUnderflowExc, i32 0, i32 0), i32 99999)\n");
  printf("ret i32 0\n");
}

int findNextLine(int lineToFind,short lineNumbers[], int lnSize)
{
  int i=0;
  while(lineNumbers[i] != lineToFind && i<lnSize)
    i++;

  if(i == lnSize) /* should never happen, but just in case */
    error("Couldn't find a line number");

  return lineNumbers[i+1];
}

void emitGoto(int dest)
{
   printf("br label %%L%d\n",dest);
   tempVarNum+=1;
}

void emitPrintExpression()
{
  printf("call void @printNumber(double %%%d)\n", tempVarNum-1);
  lastSeparatorSemicolon = 0;
}

void emitPrintStringConst(char *str)
{
  int strSize;

  addString(str);
  strSize = strlen(str);
  printf("call void @print(i8* getelementptr inbounds ([%d x i8]* @.stringConstant%d, i32 0, i32 0))\n",strSize+1,numStrings);
  lastSeparatorSemicolon = 0;
}

void emitStringVariables()
{
  int i,j;
  for(i=0;i<26;++i)
  {
    printf("@stringVariable%c = global[32 x i8] c\"",'A'+i);
    for(j=0;j<32;++j)
      printf("\\00");
    printf("\", align 1\n");
  }
}

void emitOptionBase(int base)
{
  printf("call void @setOptionBase(i32 %d)\n",base);
}

void emitDim(char *varName,int rowSize,int colSize)
{
  printf("call void @setArraySize(i8 signext %d,i32 %d,i32 %d,i32 %d)\n",(int)varName[0],rowSize,colSize,lineNumber);
}

void emitArrayLet(char *varName,int rowVar,int colVar)
{
  printf("call void @assignValue(i8 signext %d,double %%%d,double %%%d,double %%%d,i32 %d)\n",(int)varName[0],rowVar,colVar,tempVarNum-1,lineNumber);
}

void emitRetrieveArray(char *varName,int rowVar,int colVar)
{
  printf("%%%d = call double @retrieveValue(i8 signext %d,double %%%d,double %%%d,i32 %d)\n",tempVarNum,(int)varName[0],rowVar,colVar,lineNumber);
  tempVarNum+=1;
}

void emitNumericLet(char *varName)
{
  printf("store double %%%d, double* @numVar%s, align 8\n",tempVarNum-1,varName);
}

void emitNumericConstants()
{
  printf("@myZero = global double 0.000000e+00, align 8\n");
}

void emitNumericVariables()
{
  int i,j;

  printf("@retCallLineNo = global i32 0, align 4\n");
  printf("@onGotoCallLineNo = global i32 0, align 4\n");
  printf("@csTop = global i32 0, align 4\n");
  printf("@callStack = common global [%d x i32] zeroinitializer, align 4\n",CALLMAX);

  for(i=0;i<26;++i)
  {
    printf("@numVar%c = global double 0.000000e+00, align 8\n",'A'+i);
    for(j=0;j<10;++j)
      printf("@numVar%c%d = global double 0.000000e+00, align 8\n",'A'+i,j);
  }
}

void emitForLoopVariables(int numLoops)
{
  int i;

  for(i=1;i<numLoops;++i)
  {
    printf("@forLoopLimit%d = global double 0.000000e+00, align 8\n",i);
    printf("@forLoopInc%d = global double 0.000000e+00, align 8\n",i);
  }
}

void emitStringLetConstant(char varName, char *str)
{
  int strSize;

  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum, varName);
  printf("%%%d = call i8* @strcpy(i8* %%%d,",tempVarNum+1,tempVarNum);

  addString(str);
  strSize = strlen(str);

  printf("i8* getelementptr inbounds ([%d x i8]* @.stringConstant%d, i32 0, i32 0))\n",strSize+1,numStrings);

  tempVarNum += 2;
}

void emitStringLetVariable(char dest, char src)
{
  printf("%%%d = getelementptr inbounds [32 x i8]* @stringVariable%c, i32 0, i32 0\n",tempVarNum, dest);
  printf("%%%d = call i8* @strcpy(i8* %%%d,",tempVarNum+1,tempVarNum);
  printf("i8* getelementptr inbounds ([32 x i8]* @stringVariable%c, i32 0, i32 0))\n",src);

  tempVarNum += 2;
}

void emitNumericRep(double num)
{
  /* There is a better way to do this, I'm sure of it */
  printf("%%%d = load double* @myZero, align 8\n",tempVarNum);
  printf("%%%d = fadd double %%%d, ",tempVarNum+1,tempVarNum);
  printDouble(num);
  printf("\n");

  tempVarNum += 2;
}

void printDouble(double num)
{
  if(num < POS_INF && num > NEG_INF)
    printf("%.7e",num);
  else if (num > POS_INF)
    printf("1.0e100");
  else if (num < NEG_INF)
    printf("-1.0e100");
}

void emitNumericVar(char *varName)
{
  printf("%%%d = load double* @numVar%s, align 8\n",tempVarNum,varName);
  tempVarNum+=1;
}

void emitFunctionVar(char *varName)
{
  printf("%%%d = load double* %%1, align 8\n",tempVarNum);
  tempVarNum+=1;
}

void emitNegate()
{
  printf("%%%d = fsub double -0.000000e+00, %%%d\n",tempVarNum,tempVarNum-1);
  tempVarNum+=1;
}

void emitAddition(int first,int second)
{
  printf("%%%d = fadd double %%%d, %%%d\n",tempVarNum,first,second);
  tempVarNum+=1;
}

void emitSubtraction(int first,int second)
{
  printf("%%%d = fsub double %%%d, %%%d\n",tempVarNum,first,second);
  tempVarNum+=1;
}

void emitMultiplication(int first,int second)
{
  printf("%%%d = fmul double %%%d, %%%d\n",tempVarNum,first,second);
  tempVarNum+=1;
}

void emitDivision(int first,int second)
{
  printf("%%%d = call double @basicDiv(double %%%d, double %%%d, i32 %d)\n",tempVarNum,first,second,lineNumber);
  tempVarNum+=1;
}

void emitPower(int first,int second)
{
  printf("%%%d = call double @basicPow(double %%%d, double %%%d, i32 %d)\n",tempVarNum,first,second,lineNumber);
  tempVarNum+=1;
}

void emitPrintStringVar(int symTabLoc)
{
  char varName = symTable[symTabLoc].lexPtr[0];
  printf("call void @print(i8* getelementptr inbounds ([32 x i8]* @stringVariable%c, i32 0, i32 0))\n",varName);
  lastSeparatorSemicolon = 0;
}

void emitPrintingSemicolon()
{
  lastSeparatorSemicolon = 1;
}

void emitPrintingComma()
{
  printf("call void (...)* @printComma()\n");
  lastSeparatorSemicolon = 1;
}

void emitTab()
{
  printf("call void @printTab(double %%%d, i32 %d)\n",tempVarNum-1,lineNumber);
}

void emitLineNumber(int num)
{
   printf("br label %%L%d\n",num);
   printf("L%d:\n",num);
}

void emitStop()
{
   printf("call void @exit(i32 1)\n");
}

void addReturnLoc(int loc)
{
  int i;
  for(i=0;i<gsrlTop;++i)
    if(gosubReturnLocs[i] == loc)
      return;

  gosubReturnLocs[gsrlTop] = loc;
  gsrlTop++;
}

void emitStoredData()
{
  int i;

  printf("@numData = global i32 %d\n",lastData);
  printf("@storedData = global <{ ");
  for(i=0;i<lastData;++i)
  {
    printf("{ [32 x i8], double, i32 }");
    if(i+1 < lastData)
      printf(", ");
  }
  printf("}> <{");

  for(i=0;i<lastData;++i)
  {
    printf(" { [32 x i8], double, i32 } { [32 x i8] "); 
    emitStoredDataString(dataStorage[i].str);
    printf(", double ");
    printDouble(dataStorage[i].num);
    printf(", i32 %d }",dataStorage[i].isValidNum);
    if(i+1 < lastData)
      printf(", ");
  }

  printf(" }>, align 16\n");
}

void emitStoredDataString(char *str)
{
  int len,i;

  len = strlen(str);

  printf("c\"%s",str);

  for(i=len;i<32;++i)
    printf("\\00");
  printf("\"");
}

void addString(char s[])
{
  int len;
  len = strlen(s);

  if (lastStrChar + len + 1 >= STRMAX)
    error("String constants array full");

  strcpy(&constStrs[lastStrChar],s);

  numStrings+=1;
  lastStrChar = lastStrChar + len + 1;
}

void emitStringConstants()
{
  int i = 0;
  int strNum = 0;

  printf("@.retUnderflowExc = private unnamed_addr constant [16 x i8] c\"Stack underflow\\00\", align 1\n");
  printf("@.onGotoExc = private unnamed_addr constant [26 x i8] c\"On evaluated out of range\\00\", align 1\n");

  while(i != lastStrChar)
  {
    char *strPtr = &constStrs[i];
    int strSize = strlen(strPtr)+1;
    printf("@.stringConstant%d = private unnamed_addr constant [%d x i8] c\"%s\\00\", align 1\n",strNum,strSize,strPtr);
    i += strSize;
    strNum+=1;
  }
}
