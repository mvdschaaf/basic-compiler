/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __INIT__H__
#define __INIT__H__

void init();

#endif
