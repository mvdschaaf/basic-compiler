/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __PARSER__H__
#define __PARSER__H__

void parse();

typedef struct FD {
  char *basicName;
  char *asmName;
  int numArgs;
  int declaredLine;
} FuncDec;

#endif
