/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"

void error(char *m)
{
  fprintf(stderr, "Line %d: %s\n", lineNumber, m);
  exit(1);
}
