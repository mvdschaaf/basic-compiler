/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#ifndef __LEXER__H_
#define __LEXER__H_

int lexan();
void lexanRem();
char *lexanData();

#endif
