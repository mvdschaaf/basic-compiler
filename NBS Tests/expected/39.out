PROGRAM FILE 39: ACCURACY OF ADDITION
    ANSI STANDARD 7.2, 7.4, 7.6

*** NOTE: THIS PROGRAM MAKES USE OF 'READ' AND 'DATA'
    WHICH HAVE NOT YET BEEN TESTED. IF SUBSEQUENT TESTS SHOW
    THESE FEATURES TO BE INCORRECTLY IMPLEMENTED, THEN THE
    VALIDITY OF THE RESULTS OF THIS TEST ROUTINE IS DOUBTFUL.

SECTION 39.1 ACCURACY OF ADDITION.

THIS SECTION TESTS THE ACCURACY OF ADDITION. THE COMPUTED
RESULT IS COMPARED WITH A RANGE ESTABLISHED BY PERTURBING
EACH OPERAND BY 1 IN ITS 6TH DIGIT. THE RESULT PASSES IF
IT FALLS WITHIN THE EXTREME VALUES GENERATED BY THIS
PERTURBATION (ACCURATE TO SIX DIGITS).

THIS TEST IS INFORMATIVE ONLY, SINCE THE ANSI STANDARD
DOES NOT MANDATE ANY ACCURACY FOR NUMERIC EXPRESSIONS.

              BEGIN TEST

FIRST           SECOND          TRUE            COMPUTED        TEST
OPERAND         OPERAND         VALUE           VALUE           RESULT

 0               0               0               0              PASS
 0               765.432         765.432         765.432        PASS
 876.543         123.453         999.996         999.996        PASS
 0.0811111       0.0111111       0.0922222       0.0922222      PASS
-57.9999        -0.111111       -58.11101       -58.11101       PASS
 1111111         1111111         2222222         2222222        PASS
 6.54321         1.23456         7.77777         7.77777        PASS
 6.54321E-10     1.23456E-10     7.77777E-10     7.77777E-10    PASS
-6.54321E-20    -1.23456E-20    -7.77777E-20    -7.77777E-20    PASS
 6.54321E-30     1.23456E-30     7.77777E-30     7.77777E-30    PASS
 6.54321E-38     1.23456E-38     7.77777E-38     7.77777E-38    PASS
 1.23456E+10     6.54321E+10     7.77777E+10     7.77777E+10    PASS
-1.23456E+20    -6.54321E+20    -7.77777E+20    -7.77777E+20    PASS
 1.23456E+30     6.54321E+30     7.77777E+30     7.77777E+30    PASS
 1.23456E+37     6.54321E+37     7.77777E+37     7.77777E+37    PASS
 0.0499997       0.0499996       0.0999993       0.0999993      PASS
 0.0999993       4E-07           0.0999997       0.0999997      PASS
 0.0999993       4E-08           0.09999934      0.09999934     PASS
 0.0999993       4E-09           0.0999993       0.0999993      PASS
 0.0999993       4E-10           0.0999993       0.0999993      PASS
 0.0999993       4E-11           0.0999993       0.0999993      PASS
-1020304         4              -1020300        -1020300        PASS
 1020304        -304             1020000         1020000        PASS
-1020304         20304          -1000000        -1000000        PASS
-1020304         1020304         0               0              PASS
-0.1020304       4E-07          -0.10203        -0.10203        PASS
 0.1020304      -3.04E-05        0.102           0.102          PASS
-0.1020304       0.0020304      -0.1            -0.1            PASS
-0.1020304       0.1020304       0               0              PASS
-0.1020304       0.1020303      -1E-07          -1E-07          PASS
-0.1020304       0.1020305       1E-07           1E-07          PASS

*** INFORMATIVE TEST PASSED ***

               END TEST

END PROGRAM 39
