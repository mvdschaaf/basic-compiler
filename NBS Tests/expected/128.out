PROGRAM FILE 128: ACCURACY OF TAN FUNCTION.
    ANSI STANDARD 7.6, 8.4

SECTION 128.1: ACCURACY OF TAN FUNCTION.

THIS PROGRAM TESTS VALUES RETURNED BY INVOCATIONS OF THE
TAN FUNCTION FOR ACCURACY. THE INVOCATION MUST RETURN,
ACCURATE TO SIX DIGITS, SOME VALUE ACTUALLY TAKEN ON BY
THE FUNCTION WITHIN A DOMAIN BOUNDED BY THE ACTUAL ARGUMENT
PLUS OR MINUS ONE IN THE SIXTH DIGIT. ALSO, AN
'ERROR MEASURE' IS COMPUTED AND REPORTED. THIS MEASURE
IS JUST 2 * THE ABSOLUTE ERROR OVER THE SIZE OF THE RANGE;
THUS, A VALUE JUST BARELY PASSING OR FAILING WILL USUALLY
HAVE A MEASURE OF ABOUT 1. A VALUE 3 TIMES
AS INACCURATE AS EXPECTED WILL HAVE A MEASURE OF 3.

THIS TEST IS INFORMATIVE ONLY, SINCE THE ANSI STANDARD
DOES NOT MANDATE ANY ACCURACY FOR SUPPLIED-FUNCTIONS.

              BEGIN TEST

ARGUMENT        TRUE            COMPUTED        ERROR           OUTCOME
                VALUE           VALUE           MEASURE

-98778.9        -1.380175       -1.374381        0.01949705      OK 
-9876.54         0.7249919       0.7249919       9.755126E-07    OK 
-987.654        -2.526461       -2.526388        0.009999816     OK 
-98.7654        -5.06921        -5.069317        0.03992638      OK 
-9.87654        -0.4852301      -0.4852304       0.01896557      OK 
-5.55555         0.890668        0.8906678       0.008508176     OK 
-4.71239         980762.1        1087411        RANGE SPLIT      OK 
-4.56789        -6.87223        -6.87224         0.0198924       OK 
-4              -1.157821       -1.157821        0.0006214937    OK 
-3.14159         2.65359E-06     2.55359E-06     0.009999963     OK 
-2.87654         0.2714391       0.2714392       0.0101515       OK 
-2               2.18504         2.18504         0.001007349     OK 
-1.61616         22.02895        22.02895        0.0002087817    OK 
-1.57081         73135.74        73135.74        5.209496E-10    OK 
-1.5708          272241.8        279860.8       RANGE SPLIT      OK 
-1.57078        -61249.01       -61249.01        8.852899E-09    OK 
-1.23456        -2.861166       -2.861166        0.0008091404    OK 
-1              -1.557408       -1.557408        0.0006281504    OK 
-0.87654        -1.201177       -1.201177        0.008262448     OK 
-0.232323       -0.2365951      -0.2365951       0.009599714     OK 
-0.0767676      -0.07691876     -0.07691876      0.02193632      OK 
-0.0234567      -0.023461       -0.023461        0.0002449333    OK 
-0.0123456      -0.01234623     -0.01234623      0.004972276     OK 
-0.00987654     -0.009876861    -0.009876861     0.009956318     OK 
-0.00345678     -0.003456794    -0.003456794     0.004571326     OK 
-0.000987654    -0.0009876543   -0.0009876543    0.0191131       OK 
-0.000345678    -0.000345678    -0.000345678     0.009209856     OK 
-9E-05          -9E-05          -9E-05           0.001630873     OK 
-9E-07          -9E-07          -9E-07           1.614569E-07    OK 
-9E-10          -9E-10          -9E-10           0               OK  - EXACT
-9E-20          -9E-20          -9E-20           0               OK  - EXACT
-9E-30          -9E-30          -9E-30           0               OK  - EXACT
-9E-38          -9E-38          -9E-38           0               OK  - EXACT
 0               0               0              RANGE ZERO       OK  - EXACT
 9E-38           9E-38           9E-38           0               OK  - EXACT
 9E-30           9E-30           9E-30           0               OK  - EXACT
 9E-20           9E-20           9E-20           0               OK  - EXACT
 9E-10           9E-10           9E-10           0               OK  - EXACT
 9E-07           9E-07           9E-07           1.614569E-07    OK 
 9E-05           9E-05           9E-05           0.001630873     OK 
 0.000345678     0.000345678     0.000345678     0.009209856     OK 
 0.000987654     0.0009876543    0.0009876543    0.0191131       OK 
 0.00345678      0.003456794     0.003456794     0.004571326     OK 
 0.00987654      0.009876861     0.009876861     0.009956318     OK 
 0.0123456       0.01234623      0.01234623      0.004972276     OK 
 0.0234567       0.023461        0.023461        0.0002449333    OK 
 0.0767676       0.07691876      0.07691876      0.02193632      OK 
 0.232323        0.2365951       0.2365951       0.009599714     OK 
 0.876543        1.201184        1.201184        0.005530322     OK 
 1               1.557408        1.557408        0.0006281504    OK 
 1.23456         2.861166        2.861166        0.0008091404    OK 
 1.57078         61249.01        61249.01        8.852899E-09    OK 
 1.5708         -272241.8       -279860.8       RANGE SPLIT      OK 
 1.57081        -73135.74       -73135.74        5.209496E-10    OK 
 1.61616        -22.02895       -22.02895        0.0002087817    OK 
 2              -2.18504        -2.18504         0.001007349     OK 
 2.87654        -0.2714391      -0.2714392       0.0101515       OK 
 3.14159        -2.65359E-06    -2.55359E-06     0.009999963     OK 
 4               1.157821        1.157821        0.0006214937    OK 
 4.56789         6.87223         6.87224         0.0198924       OK 
 4.71239        -980762.1       -1087411        RANGE SPLIT      OK 
 5.55555        -0.890662       -0.8906678       0.3146146       OK 
 9.87654         0.4852301       0.4852304       0.01900997      OK 
 98.7654         5.06921         5.069317        0.03992638      OK 
 987.654         2.526461        2.526388        0.009999816     OK 
 9876.54        -0.7249919      -0.7249919       9.755126E-07    OK 
 98778.9         1.380175        1.374381        0.01949705      OK 

*** INFORMATIVE TEST PASSED ***

               END TEST

END PROGRAM 128
