/* Copyright (c) 2015, Matthew Vanderschaaf
   See LICENSE.txt for full license          */
#include "global.h"
#include "parser.h"
#include "init.h"

int main()
{
  init();
  parse();
  
  return 0;
}
